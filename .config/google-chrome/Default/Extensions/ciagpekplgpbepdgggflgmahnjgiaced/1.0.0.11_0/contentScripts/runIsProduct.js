var notifyIsDetail = function() {
    chrome.extension.sendMessage(
        {
            "command" : "detailPageNotify"
        }
    );
};

/*
 * Create migration IFrame: UWLMigrationFrame.
 */
var createUWLMigrationIFrame = function(doc, status, browser) {
    /*
     * Create iframe base on status info:
     *  - detect : hidden AA detection iframe;
     *  - others : normal UWL migration iframe;
     */
    try {
        var uwlMigrationFrame = doc.createElement("iframe");
        if (!status || status === '') {
            status = AMZUWLXT.Constants.UWL_STATUS_NOTICE;
        }
        var migrationSrc = "https://"
            + AMZUWLXT.Locale.getLocaleDomain()
            + "/uwlmigration?browser="
            + browser
            + "&status="
            + status
            + "&bookmarklet=false";

        var frameID = AMZUWLXT.Constants.UWL_MIGRATION_FRAME;
        if (status === AMZUWLXT.Constants.UWL_STATUS_DETECT) {
            frameID = AMZUWLXT.Constants.UWL_MIGRATION_HIDDEN_FRAME;
            uwlMigrationFrame.style.width = "0px";
            uwlMigrationFrame.style.height = "0px";
            uwlMigrationFrame.style.display = "none";
            uwlMigrationFrame.style.visibility = "hidden";
        } else {
            uwlMigrationFrame.style.overflow = "hidden";
            uwlMigrationFrame.style.width = "486px";
            if (status === AMZUWLXT.Constants.UWL_STATUS_CONFIRMATION) {
                uwlMigrationFrame.style.height = "285px";
            } else {
                uwlMigrationFrame.style.height = "325px";
            }
            uwlMigrationFrame.style.position = "absolute";
            uwlMigrationFrame.style.top = "calc(50% - 150px)";
            uwlMigrationFrame.style.left = "calc(50% - 240px)";
            uwlMigrationFrame.style.margin = "0";
            uwlMigrationFrame.style.boxShadow = "0px 0px 12px 5px gray";
            uwlMigrationFrame.style.borderWidth = "0px";
            uwlMigrationFrame.style.borderRadius = "3px";
            uwlMigrationFrame.style.zIndex = "9999";
        }
        uwlMigrationFrame.setAttribute("id", frameID);
        uwlMigrationFrame.setAttribute("src", migrationSrc);
        closeUWLMigrationIFrame(doc, frameID);
        doc.body.appendChild(uwlMigrationFrame);
    } catch (error) {
        console.log(error);
    }
};

/*
 * Close migration IFrame by id.
 */
var closeUWLMigrationIFrame = function(doc, id) {
    var iframeElem = doc.getElementById(id);
    if (iframeElem && iframeElem.parentNode) {
        iframeElem.parentNode.removeChild(iframeElem);
    }
};

/*
 * Show UWL wishlist popover view.
 */
var showUWLPopover = function(doc, showOriginalPopover, responseDomain, responseLang, browser) {
    var bookmarklet = "(function(){var w=window,l=w.location,d=w.document,s=d.createElement('script'),e=encodeURIComponent,o='object',n='AUWLBookAA__LANG__',u='https://__DOMAIN__/wishlist/add',r='readyState',T=setTimeout,a='setAttribute',g=function(){d[r]&&d[r]!='complete'?T(g,200):!w[n]?(s[a]('charset','UTF-8'),s[a]('src',u+'.js?ext=__BROWSER__&mig=true&loc='+e(l)+'&b='+n),d.body.appendChild(s),f()):f()},f=function(){!w[n]?T(f,200):w[n].showPopover({showOriginal:'true'})};typeof s!=o?l.href=u+'?u='+e(l)+'&t='+e(d.title):g()}())".replace("__DOMAIN__",responseDomain).replace("__LANG__",responseLang).replace("__BROWSER__",browser);
    if (showOriginalPopover) {
        bookmarklet = "(function(){var w=window,l=w.location,d=w.document,s=d.createElement('script'),e=encodeURIComponent,o='object',n='AUWLBookAA__LANG__',u='https://__DOMAIN__/wishlist/add',r='readyState',T=setTimeout,a='setAttribute',g=function(){d[r]&&d[r]!='complete'?T(g,200):!w[n]?(s[a]('charset','UTF-8'),s[a]('src',u+'.js?ext=__BROWSER__&loc='+e(l)+'&b='+n),d.body.appendChild(s),f()):f()},f=function(){!w[n]?T(f,200):w[n].showPopover({showOriginal:'true'})};typeof s!=o?l.href=u+'?u='+e(l)+'&t='+e(d.title):g()}())".replace("__DOMAIN__",responseDomain).replace("__LANG__",responseLang).replace("__BROWSER__",browser);
    }
    var s = doc.createElement("script");
    s.textContent = bookmarklet;
    doc.body.appendChild(s);
};

window.addEventListener("message", function(message) {
    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_CLOSE_LINK_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * Hide IFrame.
         */
        closeUWLMigrationIFrame(document, AMZUWLXT.Constants.UWL_MIGRATION_FRAME);
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_UPGRADE_NOW_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * Create UWL IFrame and show Notice view.
         */
        createUWLMigrationIFrame(document, AMZUWLXT.Constants.UWL_STATUS_NOTICE, "cr");
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_SHOW_POPOVER_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * Show UWL wishlist popover view with upgrade banner.
         */
        chrome.extension.sendMessage( {command : AMZUWLXT.Constants.UWL_LOCALEDATA_MESSAGE} , function(response) {
            /*
             * Invoke showUWLPopover(), it will either show UWL migration
             * view or original Popover view base on context info:
             *   1. Weblab not equal T1 or IE browser, it will show original Popover
             *   2. All other cases, it will show UWL migration view
             */
            showUWLPopover(document, false, response.domain, response.lang, 'chr');
        });
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_AA_DETECT_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * Will log the start time of AA detection
         */
        console.log("AA Detection(Chrome extension) start from:" + Date.now());
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.actionUrl
        && message.data.id === AMZUWLXT.Constants.UWL_CR_INLINE_INSTALL_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * For Chrome inline install, open AA landing page with extra parameter(specified by actionUrl)
         *  in new tab and trigger inline installation there.
         */
        chrome.extension.sendMessage({command : AMZUWLXT.Constants.UWL_CR_INLINE_INSTALL_MESSAGE, url: message.data.actionUrl}, function(response) {
        });
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_AA_UPGRADED_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * In case AA extension installed, send message to extension to update status.
         */
        chrome.extension.sendMessage({command : AMZUWLXT.Constants.UWL_AA_UPGRADED_MESSAGE}, function(response) {
            console.log("AA Detection(Chrome extension) Succeed at:" + Date.now());
        });
        return;
    }

    if (message.data
        && message.data.id
        && message.data.messageType
        && message.data.id === AMZUWLXT.Constants.UWL_EXT_UNINSTALL_MESSAGE
        && message.data.messageType === AMZUWLXT.Constants.UWL_MESSAGE_TYPE) {
        /*
         * Uninstall UWL extension.
         */
        chrome.extension.sendMessage({command : AMZUWLXT.Constants.UWL_EXT_UNINSTALLSELF_MESSAGE}, function(response) {
        });
        return;
    }

}, false);

chrome.extension.sendMessage(
    {
        "command" : "shouldMessage"
    },
    function(run) {
        if(run) {
            AMZUWLXT.runPage(window,notifyIsDetail);

        }
    }
);

