/*
 * Open UWL initial view inside extension or it's content script. openUWLInitialView()
 *  will leverage server side weblab to control the visibility of UWL migration feature
 *  inside UWL extension as well.
 */
var openInitialView = function(doc, domain, lang, status, browser) {
    if (!status || status === '') {
        status = AMZUWLXT.Constants.UWL_STATUS_NOTICE;
    }
    var bookmarklet = "(function() {"
        + "var w=window,l=w.location,d=w.document,s=d.createElement('script'),e=encodeURIComponent,o='object',n='AUWLBookAA__LANG__',"
        + "u='https://__DOMAIN__/wishlist/add',r='readyState',T=setTimeout,a='setAttribute',"
        + "g=function() {"
        + "d[r]&&d[r]!='complete'?T(g,200):!w[n]?(s[a]('charset','UTF-8'),"
        + "s[a]('src',u+'.js?ext=__BROWSER__&mig=true&status=__STATUS__&loc='+e(l)+'&b='+n),d.body.appendChild(s),f()):f()},"
        + "f=function(){!w[n]?T(f,200):w[n].openUWLInitialView('__STATUS__');};"
        + "typeof s!=o?l.href=u+'?u='+e(l)+'&t='+e(d.title):g()}())";
    bookmarklet = bookmarklet.replace("__DOMAIN__",domain)
        .replace("__STATUS__",status)
        .replace("__STATUS__",status)
        .replace("__LANG__", lang)
        .replace("__BROWSER__", browser);
    var oldScriptElement = doc.getElementById(AMZUWLXT.Constants.UWL_ADDJS_SCRIPT);
    if (oldScriptElement) {
        doc.body.removeChild(oldScriptElement);
    }

    var uwlAddJSScript = doc.createElement("script");
    uwlAddJSScript.setAttribute("id", AMZUWLXT.Constants.UWL_ADDJS_SCRIPT);
    uwlAddJSScript.textContent = bookmarklet;
    doc.body.appendChild(uwlAddJSScript);
};

chrome.extension.sendMessage( {"command" : "localeData"} , function(response) {
    /*
     * Invoke openInitialView(), it will either show UWL migration
     * view or original Popover view base on context info:
     *   1. Weblab not equal T1 or IE browser, it will show original Popover
     *   2. All other cases, it will show UWL migration view
     */
    openInitialView(document, response.domain, response.lang, response.status, 'chr');
});

