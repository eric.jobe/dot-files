var doRequest = function(url,callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(state) {
        if(req.readyState === 4 && (req.status === 200 || req.status === 0)) {
            callback(req.responseText);               
        }
    };
    req.open("GET", url, true);
    req.overrideMimeType('text/plain; charset=us-ascii');
    req.send(null);
};

var updatePushDownDate = function(reset) {
    var settings = AMZUWLXT.Settings.get();		
    var deltaDays = [0, 30, 90, 180, 300];		
    var defaultDelta = 30;
      
    var curPushInterval = settings.pushInterval;
    var newPushInterval;
    if (reset) {
        newPushInterval = "1";
    } else if (curPushInterval == deltaDays.length - 1) {
        newPushInterval = curPushInterval;
    } else {
        var curPushIntervalInteger = parseInt(curPushInterval) || 0;
        newPushInterval = curPushIntervalInteger + 1;
    }
                         
    AMZUWLXT.Settings.set({"pushInterval" : newPushInterval});	
    
    var nextDate = new Date();		
    var deltaDuration = (typeof deltaDays[newPushInterval] !== "undefined") ? deltaDays[newPushInterval] : defaultDelta;
    nextDate.setDate(nextDate.getDate() + deltaDuration);
    AMZUWLXT.Settings.set({"pushDate" : nextDate});	
};

var updateBadge = function(tabId) {
                chrome.browserAction.setBadgeBackgroundColor({ color : [9,175,216,255],
                                                               tabId: tabId});
                chrome.browserAction.setBadgeText({ text : "+",
                                                    tabId : tabId});

};

var updateBrowserAction = function(tabId) {
    var canvas = document.createElement("canvas");
    var srcImage = new Image();
    var settings = AMZUWLXT.Settings.get();
    srcImage.onload = function() {

        canvas.width = 19;
        canvas.height = 19;
        var context = canvas.getContext('2d');
        
        var frameNumber = 0;
        var runFrame = function(i) {
            context.clearRect(0,0,21,21);
            context.drawImage(srcImage,i * 21 + 0 ,0,19,19,0,0,19,19);
            var iData = context.getImageData(0,0,19,19);
            chrome.browserAction.setIcon({ imageData : iData,
                                           tabId : tabId});

            if((i + 1) * 21 < srcImage.width) {
                setTimeout(function() {
                    runFrame(i+1);
                },50);
            } else {
                updateBadge(tabId);
            }
            context.restore();
            
        };

        if(settings.notify) {
            runFrame(0);
        }
    };

    srcImage.src = chrome.extension.getURL("images/glow.png");
};

/*
 * Load UWL Chrome extension migration status from extension local storage: notice,
 *   retired or upgraded.
 * When install AA and upgrade from Horizonte view, Migration action will send message
 *   to extension to update it's migration status.
 */
var loadMigrationStatus = function() {
    var migrationStatus = '';
    try {
        migrationStatus = localStorage.getItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY);
    } catch (error) {
        console.log(error);
    }
    var retireTimeline = Date.parse(AMZUWLXT.Settings.constants.UWL_RETIRED_DATE);
    if (!migrationStatus) {
        migrationStatus = AMZUWLXT.Settings.constants.UWL_STATUS_NOTICE;
    }
    if (migrationStatus === AMZUWLXT.Settings.constants.UWL_STATUS_NOTICE && retireTimeline < Date.now()) {
        migrationStatus = AMZUWLXT.Settings.constants.UWL_STATUS_RETIRED;
    }
    try {
        localStorage.setItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY, migrationStatus);
    } catch (error) {
        console.log(error);
    }

    return migrationStatus;
};

chrome.extension.onMessage.addListener(

    function(request, sender, sendResponse) {
        var settings = AMZUWLXT.Settings.get();

        if(request.command === "localeData") {
            sendResponse(
                {
                    "lang"   : AMZUWLXT.Locale.getLanguageMapping(),
                    "domain" : AMZUWLXT.Locale.getLocaleDomain(),
                    "status" : loadMigrationStatus()
                }
            );
            updatePushDownDate(true);
            return;
        }

        if(request.command === AMZUWLXT.Settings.constants.UWL_AA_UPGRADED_MESSAGE) {
            try {
                localStorage.setItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY, AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED);
            } catch (error) {
                console.log(error);
            }

            sendResponse(
                {
                    "lang"   : AMZUWLXT.Locale.getLanguageMapping(),
                    "domain" : AMZUWLXT.Locale.getLocaleDomain(),
                    "status" : AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED
                }
            );
            return;
        }

        if(request.command === AMZUWLXT.Settings.constants.UWL_CR_INLINE_INSTALL_MESSAGE) {
            chrome.tabs.create({url:request.url});
            return;
        }

        if(request.command === AMZUWLXT.Settings.constants.UWL_AA_UPGRADED_MESSAGE) {
            try {
                localStorage.setItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY, AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED);
            } catch (error) {
                console.log(error);
            }

            sendResponse(
                {
                    "lang"   : AMZUWLXT.Locale.getLanguageMapping(),
                    "domain" : AMZUWLXT.Locale.getLocaleDomain(),
                    "status" : AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED
                }
            );
            return;
        }

        if(request.command === AMZUWLXT.Settings.constants.UWL_CR_INLINE_INSTALL_MESSAGE) {
            chrome.tabs.create({url:request.url});
            return;
        }

        if(request.command === "shouldMessage") {
            sendResponse(settings.notify ||
                         settings.push ||
                         !settings.pushed);
            return;
        }

        if (request.command === AMZUWLXT.Settings.constants.UWL_EXT_UNINSTALLSELF_MESSAGE) {
            chrome.management.uninstallSelf();

            try {
                if (AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED !== localStorage.getItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY)) {
                    localStorage.setItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY, AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED);
                }
            } catch (error) {
                console.log(error);
            }
            return;
        }

        if (request.command === AMZUWLXT.Settings.constants.UWL_EXT_UNINSTALLSELF_MESSAGE) {
            chrome.management.uninstallSelf();

            try {
                if (AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED !== localStorage.getItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY)) {
                    localStorage.setItem(AMZUWLXT.Settings.constants.UWL_MIGRATION_STATUS_KEY, AMZUWLXT.Settings.constants.UWL_STATUS_UPGRADED);
                }
            } catch (error) {
                console.log(error);
            }
            return;
        }

        if(request.command === "markPushed") {
            AMZUWLXT.Settings.set( {"pushed" : true } );
            updatePushDownDate(false);
            return;
        }

        if(request.command === "detailPageNotify") {
            updateBrowserAction(sender.tab.id);
            if(settings.push || !settings.pushed || settings.pushDate < new Date()) {
                chrome.tabs.executeScript(sender.tab.id,
                                          {file : 'contentScripts/pushDownContent.js'});
                chrome.tabs.executeScript(sender.tab.id,
                                          {file : 'contentScripts/pushDownRun.js'});
            }
            return;
        }

        if(request.command === "pushdown") {

            doRequest(chrome.extension.getURL("pushdown.html"), function(pushdownHTML) {
                doRequest(chrome.extension.getURL("expander.html"), function(expanderHTML) {
                    doRequest(chrome.extension.getURL("linktemplate.html"), function(linkHTML) {
                        var domain = AMZUWLXT.Locale.getLocaleDomain();
                        var vars = {"ext-path" : chrome.extension.getURL(""),
                                    "wishlist-link" : 
                                    AMZUWLXT.Strings.process(
                                        linkHTML,
                                        {
                                            "href" : "https://" + domain + "/wishlist/ref=cm_wlext_chr_pd_wl",
                                            "body" : AMZUWLXT.Strings.lookup("uwl-ext-chrome-pushdown-content-linktext")
                                        }
                                    )};

                        pushdownHTML = AMZUWLXT.Strings.process(
                            pushdownHTML,
                            vars);


                        sendResponse({ "pushDownHtml" : pushdownHTML,
                                       "expanderHtml" : expanderHTML});

                    });
                });
            })
            return;
        }
    }
);



