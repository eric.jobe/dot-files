chrome.browserAction.onClicked.addListener(function(tab) {
    if (tab && tab.status === 'complete') {
        chrome.tabs.executeScript(tab.id, {
            allFrames: false,
            file: "contentScripts/tests.js"  });
        chrome.tabs.executeScript(tab.id, {
            allFrames: false,
            file: "contentScripts/inject_bookmarklet.js"  });
    }
});
