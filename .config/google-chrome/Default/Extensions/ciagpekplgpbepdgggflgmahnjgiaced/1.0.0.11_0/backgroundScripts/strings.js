AMZUWLXT.Strings.strings = {
   "uwl-ext-chrome-pushdown-content-chrome" : {
      "en" : "Whenever you visit a product page, you'll see the &quot;Add to Amazon Wish List&quot; button animate in the top right corner of your browser as a reminder that you can add any item, from any website to your ${wishlist-link}",
      "fr" : "Chaque fois que vous vous rendrez sur une page produit, vous verrez le bouton &laquo;&nbsp;Ajouter &agrave; votre Liste d'envies Amazon&nbsp;&raquo; s'animer  dans le coin situ&eacute; en haut &agrave; droite de votre navigateur. Cette animation a pour but de vous rappeler que vous pouvez ajouter &agrave; votre ${wishlist-link} n'importe quel article depuis n'importe quel site Internet.",
      "it" : "Ogni volta che si visita una pagina prodotto il pulsante &quot;Aggiungi alla Lista Desideri&quot;, che si trova nella parte superiore destra della pagina si animer&agrave;. Questo per ricordare che &egrave; possibile aggiungere alla propria ${wishlist-link} prodotti da qualsiasi sito web.",
      "de" : "Immer wenn Sie eine Produktseite besuchen, erscheint der Button &quot;Auf meinen Wunschzettel&quot; oben in Ihrem Browser. Er erinnert Sie daran, dass Sie jeden beliebigen Artikel von jeder Website auf Ihren ${wishlist-link} setzen k&ouml;nnen.",
      "ja" : "&#x307B;&#x304B;&#x306E;&#x30B5;&#x30A4;&#x30C8;&#x306E;&#x5546;&#x54C1;&#x30DA;&#x30FC;&#x30B8;&#x306B;&#x884C;&#x304F;&#x3068;&#x3001;&#x30D6;&#x30E9;&#x30A6;&#x30B6;&#x53F3;&#x4E0A;&#x306E;&#x300C;&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;&#x306B;&#x8FFD;&#x52A0;&#x3059;&#x308B;&#x300D;&#x3068;&#x3044;&#x3046;&#x30DC;&#x30BF;&#x30F3;&#x306B;&#x300C;+&#x300D;&#x306E;&#x8A18;&#x53F7;&#x304C;&#x30A2;&#x30CB;&#x30E1;&#x30FC;&#x30B7;&#x30E7;&#x30F3;&#x8868;&#x793A;&#x3055;&#x308C;&#x307E;&#x3059;&#x3002;&#x3053;&#x306E;&#x8A18;&#x53F7;&#x306B;&#x3088;&#x3063;&#x3066;&#x3001;&#x5546;&#x54C1;&#x3092;&#x898B;&#x9003;&#x3059;&#x3053;&#x3068;&#x306A;&#x304F; ${wishlist-link} &#x306B;&#x8FFD;&#x52A0;&#x3059;&#x308B;&#x3053;&#x3068;&#x304C;&#x3067;&#x304D;&#x307E;&#x3059;&#x3002;",
      "zh" : "&#x5F53;&#x60A8;&#x8BBF;&#x95EE;&#x4EFB;&#x4F55;&#x4E00;&#x4E2A;&#x7F51;&#x7AD9;&#x7684;&#x5546;&#x54C1;&#x8BE6;&#x60C5;&#x9875;&#x65F6;&#xFF0C;&#x60A8;&#x90FD;&#x53EF;&#x4EE5;&#x770B;&#x5230;&#x6D4F;&#x89C8;&#x5668;&#x7684;&#x53F3;&#x4E0A;&#x89D2;&#x51FA;&#x73B0;&ldquo;&#x52A0;&#x5165;&#x5FC3;&#x613F;&#x5355;&rdquo;&#x6309;&#x94AE;&#x7684;&#x52A8;&#x6001;&#x63D0;&#x793A;&#xFF0C;&#x63D0;&#x9192;&#x60A8;&#x53EF;&#x4EE5;&#x5C06;&#x4EFB;&#x4F55;&#x7F51;&#x7AD9;&#x7684;&#x4EFB;&#x4F55;&#x5546;&#x54C1;&#x6DFB;&#x52A0;&#x5230;&#x60A8;&#x7684;&#x5353;&#x8D8A;&#x4E9A;&#x9A6C;&#x900A; ${wishlist-link}&#x3002;",
      "en_gb" : "Whenever you visit a product page, you'll see the &quot;Add to Amazon Wish List&quot; button animate in the top right corner of your browser as a reminder that you can add any item, from any website to your ${wishlist-link}"
   },
   "uwl-ext-location" : {
      "en" : "Location",
      "fr" : "Adresse",
      "it" : "Posizione",
      "de" : "Website",
      "ja" : "&#x30ED;&#x30B1;&#x30FC;&#x30B7;&#x30E7;&#x30F3;",
      "zh" : "&#x7F51;&#x5740;",
      "en_gb" : "Location"
   },
   "uwl-ext-chrome-pushdown-content-linktext" : {
      "en" : "Amazon Wish List.",
      "fr" : "Liste d'envies Amazon",
      "it" : "Lista Desideri Amazon",
      "de" : "Amazon-Wunschzettel",
      "ja" : "Amazon.co.jp&#x306E;&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;",
      "zh" : "&#x5353;&#x8D8A;&#x4E9A;&#x9A6C;&#x900A;&#x5FC3;&#x613F;&#x5355;",
      "en_gb" : "Amazon Wish List."
   },
   "uwl-ext-pushdown-settings-text" : {
      "en" : "Settings",
      "fr" : "Param&egrave;tres",
      "it" : "Impostazioni",
      "de" : "Einstellungen",
      "ja" : "&#x8A2D;&#x5B9A;",
      "zh" : "&#x8BBE;&#x7F6E;",
      "en_gb" : "Settings"
   },
   "uwl-ext-copy-footer" : {
      "en" : "&copy; 1996-2010, Amazon.com, Inc. or its affiliates",
      "fr" : "&copy; 1996-2010, Amazon.com, Inc. ou ses filiales.",
      "it" : "&copy; 2010-2011, Amazon.com, Inc. o societ&agrave; affiliate.",
      "de" : "&copy; 1998-2011, Amazon.com, Inc. oder Tochtergesellschaften",
      "ja" : "&copy; 1996-2010, Amazon.com, Inc. or its affiliates",
      "zh" : "&copy; 1999-2011, Amazon Joyo Co., Ltd.",
      "en_gb" : "&#xFFFD; 1996-2011, Amazon.com, Inc. or its affiliates"
   },
   "uwl-ext-set-default-location" : {
      "en" : "Set default Wish List location to:",
      "fr" : "Adresse &agrave; utiliser par d&eacute;faut pour cette Liste d'envies&nbsp;:",
      "it" : "Imposta posizione predefinita per Lista Desideri:",
      "de" : "Standard-Website f&uuml;r den Wunschzettel:",
      "ja" : "&#x6B21;&#x306E;&#x30ED;&#x30B1;&#x30FC;&#x30B7;&#x30E7;&#x30F3;&#x3092;&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;&#x306E;&#x30C7;&#x30D5;&#x30A9;&#x30EB;&#x30C8;&#x306B;&#x8A2D;&#x5B9A;&#x3059;&#x308B;:",
      "zh" : "&#x628A;&#x5FC3;&#x613F;&#x5355;&#x7684;&#x9ED8;&#x8BA4;&#x7F51;&#x5740;&#x8BBE;&#x7F6E;&#x4E3A;&#xFF1A;",
      "en_gb" : "Set default Wish List location to:"
   },
   "uwl-ext-chrome-enable-animation" : {
      "en" : "Enable the animation of the Add to Wish List button image on product pages",
      "fr" : "Activer l'animation du bouton &laquo;&nbsp;Ajouter &agrave; votre Liste d'envies&nbsp;&raquo; sur les pages produits",
      "it" : "Abilita l'animazione del pulsante &quot;Aggiungi alla Lista Desideri&quot; sulle pagine prodotto",
      "de" : "Animierte Darstellung des &quot;Auf meinen Wunschzettel&quot;-Buttons auf Produktseiten aktivieren",
      "ja" : "&#x5546;&#x54C1;&#x30DA;&#x30FC;&#x30B8;&#x3067;&#x3001;&#x300C;&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;&#x306B;&#x8FFD;&#x52A0;&#x3059;&#x308B;&#x300D;&#x30DC;&#x30BF;&#x30F3;&#x306E;&#x30A2;&#x30CB;&#x30E1;&#x30FC;&#x30B7;&#x30E7;&#x30F3;&#x3092;&#x6709;&#x52B9;&#x306B;&#x3059;&#x308B;",
      "zh" : "&#x5728;&#x5546;&#x54C1;&#x8BE6;&#x7EC6;&#x9875;&#x6FC0;&#x6D3B;&ldquo;&#x52A0;&#x5165;&#x5FC3;&#x613F;&#x5355;&rdquo;&#x7684;&#x52A8;&#x6001;&#x63D0;&#x793A;",
      "en_gb" : "Enable the animation of the Add to Wish List button image on product pages"
   },
   "uwl-ext-product-cue" : {
      "en" : "Product Page Cue",
      "fr" : "Pour signaler les pages produits",
      "it" : "Cue pagina prodotto",
      "de" : "Hinweis auf Produktseite",
      "ja" : "&#x5546;&#x54C1;&#x30DA;&#x30FC;&#x30B8;&#x3067;&#x306E;&#x8868;&#x793A;",
      "zh" : "&#x5546;&#x54C1;&#x8BE6;&#x7EC6;&#x9875;&#x63D0;&#x793A;",
      "en_gb" : "Product Page Cue"
   },
   "uwl-ext-chrome-description" : {
      "en" : "Add products from any website to your Amazon Wish List",
      "fr" : "Ajoutez des articles à votre Liste d'envies Amazon depuis n'importe quel site Internet",
      "it" : "Aggiungi prodotti da qualsiasi sito alla tua Lista Desideri Amazon",
      "de" : "Fügen Sie Produkte von jeder beliebigen Website Ihrem Wunschzettel bei Amazon hinzu",
      "ja" : "\u307B\u304B\u306E\u30B5\u30A4\u30C8\u306E\u5546\u54C1\u3092Amazon.co.jp\u306E\u307B\u3057\u3044\u7269\u30EA\u30B9\u30C8\u306B\u8FFD\u52A0\u3067\u304D\u307E\u3059",
      "zh" : "\u5C06\u4EFB\u610F\u7F51\u7AD9\u7684\u5546\u54C1\u6DFB\u52A0\u5230\u4F60\u7684\u5353\u8D8A\u4E9A\u9A6C\u900A\u5FC3\u613F\u5355",
      "en_gb" : "Add products from any website to your Amazon Wish List"
   },
   "uwl-ext-pushdown-header" : {
      "en" : "Add This Item to Your Amazon Wish List",
      "fr" : "Ajoutez cet article &agrave; votre Liste d'envies Amazon",
      "it" : "Aggiungi questo articolo alla Lista Desideri Amazon",
      "de" : "Diesen Artikel Ihrem Wunschzettel bei Amazon hinzuf&uuml;gen",
      "ja" : "&#x3053;&#x306E;&#x5546;&#x54C1;&#x3092;Amazon.co.jp&#x306E;&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;&#x306B;&#x8FFD;&#x52A0;&#x3059;&#x308B;",
      "zh" : "&#x5C06;&#x6B64;&#x5546;&#x54C1;&#x6DFB;&#x52A0;&#x5230;&#x60A8;&#x7684;&#x5353;&#x8D8A;&#x4E9A;&#x9A6C;&#x900A;&#x5FC3;&#x613F;&#x5355;",
      "en_gb" : "Add This Item to Your Amazon Wish List"
   },
   "uwl-ext-saved" : {
      "en" : "Saved",
      "fr" : "Param&egrave;tres enregistr&eacute;s",
      "it" : "Salvata",
      "de" : "Gespeichert",
      "ja" : "&#x4FDD;&#x5B58;&#x3057;&#x307E;&#x3057;&#x305F;",
      "zh" : "&#x5DF2;&#x4FDD;&#x5B58;",
      "en_gb" : "Saved"
   },
   "uwl-ext-chrome-browseraction" : {
      "en" : "Add to Amazon Wish List",
      "fr" : "Ajouter à votre Liste d'envies Amazon",
      "it" : "Aggiungi alla Lista Desideri Amazon",
      "de" : "Auf den Amazon-Wunschzettel",
      "ja" : "Amazon.co.jp\u306E\u307B\u3057\u3044\u7269\u30EA\u30B9\u30C8\u306B\u8FFD\u52A0",
      "zh" : "\u52A0\u5165\u5353\u8D8A\u4E9A\u9A6C\u900A\u5FC3\u613F\u5355",
      "en_gb" : "Add to Amazon Wish List"
   },
   "uwl-ext-chrome-settings-title" : {
      "en" : "Amazon Wish List Extension Settings",
      "fr" : "Param&egrave;tres de l'extension Liste d'envies Amazon",
      "it" : "Impostazione estensione Lista Desideri Amazon",
      "de" : "Erweiterte Einstellungen f&uuml;r den Amazon-Wunschzettel",
      "ja" : "&#x307B;&#x3057;&#x3044;&#x7269;&#x30EA;&#x30B9;&#x30C8;&#x62E1;&#x5F35;&#x6A5F;&#x80FD;&#x8A2D;&#x5B9A;",
      "zh" : "&#x5353;&#x8D8A;&#x4E9A;&#x9A6C;&#x900A;&#x5FC3;&#x613F;&#x5355;&#x6269;&#x5C55;&#x529F;&#x80FD;&#x8BBE;&#x7F6E;",
      "en_gb" : "Amazon Wish List Extension Settings"
   },
   "uwl-ext-chrome-name" : {
      "en" : "Add to Amazon Wish List",
      "fr" : "Ajouter à votre Liste d'envies Amazon",
      "it" : "Aggiungi alla Lista Desideri Amazon",
      "de" : "Auf den Amazon-Wunschzettel",
      "ja" : "Amazon.co.jp\u306E\u307B\u3057\u3044\u7269\u30EA\u30B9\u30C8\u306B\u8FFD\u52A0",
      "zh" : "\u52A0\u5165\u5353\u8D8A\u4E9A\u9A6C\u900A\u5FC3\u613F\u5355",
      "en_gb" : "Add to Amazon Wish List"
   }
}