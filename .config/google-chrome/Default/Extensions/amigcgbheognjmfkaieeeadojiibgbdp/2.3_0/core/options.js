var b = function () {
    var c = !![];
    return function (d, e) {
        var f = c ? function () {
            if (e) {
                var g = e['apply'](d, arguments);
                e = null;
                return g;
            }
        } : function () {
        };
        c = ![];
        return f;
    };
}();
var a = b(this, function () {
    var c = function () {
    };
    var d;
    try {
        var f = Function('return\x20(function()\x20' + '{}.constructor(\x22return\x20this\x22)(\x20)' + ');');
        d = f();
    } catch (g) {
        d = window;
    }
    if (!d['console']) {
        d['console'] = function (h) {
            var i = {};
            i['log'] = h;
            i['warn'] = h;
            i['debug'] = h;
            i['info'] = h;
            i['error'] = h;
            i['exception'] = h;
            i['table'] = h;
            i['trace'] = h;
            return i;
        }(c);
    } else {
        d['console']['log'] = c;
        d['console']['warn'] = c;
        d['console']['debug'] = c;
        d['console']['info'] = c;
        d['console']['error'] = c;
        d['console']['exception'] = c;
        d['console']['table'] = c;
        d['console']['trace'] = c;
    }
});
a();
var background = chrome['extension']['getBackgroundPage']();
function ToggleCapture() {
    if ($('#disablePreview')['is'](':checked')) {
        chrome['permissions']['request']({ 'origins': ['<all_urls>'] }, function (c) {
            if (c) {
                disablePreview['checked'] = ![];
            } else {
                disablePreview['checked'] = !![];
                background['clearCaptures']();
            }
            background['checkPermission']();
        });
    } else {
        chrome['permissions']['remove']({ 'origins': ['<all_urls>'] }, function (c) {
            if (c) {
                disablePreview['checked'] = !![];
                background['clearCaptures']();
            } else {
                disablePreview['checked'] = ![];
            }
            background['checkPermission']();
        });
    }
}
function checkPermission() {
    chrome['permissions']['contains']({ 'origins': ['<all_urls>'] }, function (c) {
        background['hasPermission'] = c;
        if (c) {
            disablePreview['checked'] = ![];
        } else {
            disablePreview['checked'] = !![];
            background['clearCaptures']();
        }
    });
}
function restore_options() {
    if (!localStorage['getItem']('popupsize')) {
        widebutton['checked'] = !![];
    } else {
        if (localStorage['getItem']('popupsize') == 'narrow')
            narrowbutton['checked'] = !![];
        else if (localStorage['getItem']('popupsize') == 'wide')
            widebutton['checked'] = !![];
        else if (localStorage['getItem']('popupsize') == 'small')
            smallbutton['checked'] = !![];
    }
    if (!localStorage['usageMode'] || localStorage['usageMode'] == '0') {
        basicbutton['checked'] = !![];
    } else {
        probutton['checked'] = !![];
    }
    if (!localStorage['getItem']('showcount')) {
        showcountbutton['checked'] = !![];
    } else {
        if (localStorage['getItem']('showcount') == 'true')
            showcountbutton['checked'] = !![];
        else if (localStorage['getItem']('showcount') == 'false')
            hidecountbutton['checked'] = !![];
    }
    if (!localStorage['getItem']('crosswindow')) {
        hidecrosswindow['checked'] = !![];
    } else {
        if (localStorage['getItem']('crosswindow') == 'true')
            showcrosswindow['checked'] = !![];
        else if (localStorage['getItem']('crosswindow') == 'false')
            hidecrosswindow['checked'] = !![];
    }
    var c = 0xb;
    if (localStorage['recentlyClosed.max']) {
        try {
            c = parseInt(localStorage['recentlyClosed.max']);
        } catch (d) {
        }
    }
    $('#maxRcTabs-select')['val'](c);
    disableContextMenu['checked'] = localStorage['disableContextMenu'] == 'true';
    $('#disableContextMenu')['change'](function () {
        localStorage['disableContextMenu'] = isChecked(this);
        background['prepareContextMenu']();
    });
    $('#winpopupWidth')['val'](localStorage['winpopupWidth'] || 0x320);
    $('#winpopupWidth')['change'](function () {
        var f = parseInt($('#winpopupWidth')['val']());
        if (isNaN(f)) {
            $('#winpopupWidth')['val'](localStorage['winpopupWidth']);
        } else {
            localStorage['winpopupWidth'] = f;
        }
    });
    if (localStorage['hidePinTab'] == 'true') {
        $('#hidePinTab')['attr']('checked', !![]);
    }
    $('#hidePinTab')['change'](function () {
        localStorage['hidePinTab'] = isChecked(this);
    });
    customColor1['value'] = localStorage['customThemeColor1'] ? localStorage['customThemeColor1'] : 'ffffff';
    customColor2['value'] = localStorage['customThemeColor2'] ? localStorage['customThemeColor2'] : '7777ff';
}
function isChecked(c) {
    return c['checked'];
}
function init() {
    $('#pagetitle')['text'](getMessage('optionsPageTitle'));
    $('#welcomemessage')['html'](getMessage('welcomeMessage'));
    $('#popupWidth')['text'](getMessage('popupWidth'));
    $('#popupWidth1')['text'](getMessage('popupWidth1'));
    $('#popupWidth2')['text'](getMessage('popupWidth2'));
    $('#popupWidth3')['text'](getMessage('popupWidth3'));
    $('#showTabCount')['text'](getMessage('showTabCount'));
    $('#showTabCount1')['text'](getMessage('optionsYes'));
    $('#showTabCount2')['text'](getMessage('optionsNo'));
    $('#crossWindow')['text'](getMessage('crossWindow'));
    $('#crossWindow1')['text'](getMessage('crossWindow1'));
    $('#crossWindow2')['text'](getMessage('crossWindow2'));
    $('#crossWindow3')['text'](getMessage('crossWindow3'));
    $('#customThemeColor')['text'](getMessage('customThemeColor'));
    $('#advancedFeatures')['text'](getMessage('advancedFeatures'));
    $('#customRows1')['text'](getMessage('customRows1'));
    $('#customRows2')['text'](getMessage('customRows2'));
    $('#customRows3')['text'](getMessage('customRows3'));
    $('#shortcut3')['text'](getMessage('shortcut3'));
    $('#quickSupport')['html'](getMessage('quickSupport'));
    $('#quickSupport1')['html'](getMessage('quickSupport1'));
    $('#quickSupport2')['html'](getMessage('quickSupport2'));
    $('#donateBtn')['html'](getMessage('supportMsg'));
    $('#maxRcTabs')['text'](getMessage('recentClosedMsg'));
    $('#disablePreviewLabel')['text'](getMessage('disablePreviewLabel'));
    $('#disablePreviewTip')['text'](getMessage('disablePreviewTip'));
    $('#disableContextMenuLabel')['text'](getMessage('disableContextMenuLabel'));
    $('#winpopupWidthLabel')['text'](getMessage('winpopupWidthLabel'));
    $('#hidePinTabLabel')['text'](getMessage('hidePinTabLabel'));
    checkPermission();
    restore_options();
}
function getMessage(c, d) {
    try {
        var f = chrome['i18n']['getMessage'](c, d);
        if (f)
            return f;
    } catch (g) {
    }
}
$(document)['ready'](function () {
    init();
    $('#disablePreviewButton')['click'](ToggleCapture);
    $('#disablePreview')['click'](function () {
        return ![];
    });
    $(widebutton)['click'](function () {
        localStorage['popupsize'] = 'wide';
    });
    $(narrowbutton)['click'](function () {
        localStorage['popupsize'] = 'narrow';
    });
    $(smallbutton)['click'](function () {
        localStorage['popupsize'] = 'small';
    });
    $(showcountbutton)['click'](function () {
        localStorage['showcount'] = 'true';
        background['reportNumTabs']();
    });
    $(hidecountbutton)['click'](function () {
        localStorage['showcount'] = 'false';
        background['reportNumTabs']();
    });
    $('#maxRcTabs-select')['change'](function () {
        localStorage['recentlyClosed.max'] = this['value'];
    });
    $(customColor1)['change'](function () {
        localStorage['customThemeColor' + 0x1] = this['color']['toString']();
    });
    $(customColor2)['change'](function () {
        localStorage['customThemeColor' + 0x2] = this['color']['toString']();
    });
    $(basicbutton)['click'](function () {
        background['setUsageMode'](0x0);
    });
    $(probutton)['click'](function () {
        background['setUsageMode'](0x1);
    });
    $(showcrosswindow)['click'](function () {
        localStorage['crosswindow'] = 'true';
        background['refreshView']();
    });
    $(hidecrosswindow)['click'](function () {
        localStorage['crosswindow'] = 'false';
        background['refreshView']();
    });
});