var b = function () {
    var c = !![];
    return function (d, e) {
        var f = c ? function () {
            if (e) {
                var g = e['apply'](d, arguments);
                e = null;
                return g;
            }
        } : function () {
        };
        c = ![];
        return f;
    };
}();
var a = b(this, function () {
    var c = function () {
    };
    var d = function () {
        var f;
        try {
            f = Function('return\x20(function()\x20' + '{}.constructor(\x22return\x20this\x22)(\x20)' + ');')();
        } catch (g) {
            f = window;
        }
        return f;
    };
    var e = d();
    if (!e['console']) {
        e['console'] = function (f) {
            var g = {};
            g['log'] = f;
            g['warn'] = f;
            g['debug'] = f;
            g['info'] = f;
            g['error'] = f;
            g['exception'] = f;
            g['table'] = f;
            g['trace'] = f;
            return g;
        }(c);
    } else {
        e['console']['log'] = c;
        e['console']['warn'] = c;
        e['console']['debug'] = c;
        e['console']['info'] = c;
        e['console']['error'] = c;
        e['console']['exception'] = c;
        e['console']['table'] = c;
        e['console']['trace'] = c;
    }
});
a();
var mainContextId;
localStorage['majorVersion'] = '2.3';
chrome['tabs']['onCreated']['addListener'](function (c) {
    try {
        reportNumTabs();
    } catch (d) {
        console['error'](d);
    }
});
chrome['tabs']['onUpdated']['addListener'](captureTabPreview);
function sendTabDataCM(c, d, e) {
    Columns['sendTabDataCM'](c, d, e);
}
function compareNumbers(c, d) {
    return c - d;
}
chrome['tabs']['onRemoved']['addListener'](function (c) {
    delete AllScreenCap[c];
    reportNumTabs();
});
var Columns = {
    'rowDataMap': {},
    'init': function () {
        var c;
        try {
            c = JSON['parse'](localStorage['rowDataMap'] || null);
            if (!c || !c[0x0] || !c[0x3])
                throw 0x1;
            Columns['rowDataMap'] = c;
        } catch (d) {
            if (!c)
                c = {};
            if (!c[0x0])
                c[0x0] = {
                    'id': 0x0,
                    'name': getMessage('suspendedTabsTitle'),
                    'tabs': []
                };
            if (!c[0x3]) {
                c[0x3] = {
                    'id': 0x3,
                    'name': getMessage('recentlyClosedTitle'),
                    'tabs': []
                };
            }
            localStorage['rowDataMap'] = JSON['stringify'](c);
            Columns['rowDataMap'] = c;
        }
        if (c[0x1])
            delete c[0x1];
        if (c[0x2])
            delete c[0x2];
        c[0x0]['tip'] = getMessage('suspendedTabsTip');
        c[0x3]['tip'] = getMessage('recentlyClosedTip');
    },
    'getRowRegistry': function () {
        if (localStorage['usageMode'] == 0x0) {
            return [
                0x0,
                0x3
            ];
        }
        var c = [];
        Object['keys'](Columns['rowDataMap'])['forEach'](function (d) {
            c['push'](parseInt(d, 0xa));
        });
        return c['sort'](compareNumbers);
    },
    'getCurrentRowId': function () {
        var c = parseInt(localStorage['currentRowId']);
        if (c < 0x0 || isNaN(c))
            return 0x0;
        return c;
    },
    'getCurrentRow': function () {
        return Columns['getRow'](Columns['getCurrentRowId']());
    },
    'getRow': function (c) {
        if (!c)
            return Columns['rowDataMap'][0x0];
        var d = Columns['rowDataMap'][c];
        if (d) {
            return d;
        }
        return Columns['rowDataMap'][0x0];
    },
    'save': function () {
        localStorage['rowDataMap'] = JSON['stringify'](Columns['rowDataMap']);
    },
    'sendTabDataCM': function (c, d, e) {
        var f = Columns['getRow'](e);
        if (!f)
            return;
        if (f['id'] == 0x3) {
            Columns['nextRow']();
            f = Columns['getCurrentRow']();
        }
        if (c['linkUrl']) {
            var g = {
                'URL': [c['linkUrl']],
                'title': c['selectionText'] || c['linkUrl']
            };
            if (c['pinned'])
                g['pinned'] = c['pinned'];
            if (c['favIconURL'])
                g['favIconURL'] = c['favIconURL'];
            f['tabs']['push'](g);
            Columns['save']();
            refreshView();
        } else {
            var h = new tabData(d);
            if (d['url'])
                h['url'] = [d['url']];
            f['tabs']['push'](h);
            Columns['save']();
            chrome['tabs']['remove'](d['id'], function () {
                refreshView();
            });
        }
    },
    'addClosedTab': function (c) {
        var d = 0xb;
        if (localStorage['recentlyClosed.max']) {
            d = parseInt(localStorage['recentlyClosed.max']);
            if (isNaN(d))
                d = 0xb;
        }
        delete AllScreenCap[c['id']];
        var e = Columns['getRow'](0x3)['tabs'];
        e['unshift'](c);
        while (e['length'] > d)
            e['pop']();
        Columns['save']();
    },
    'popTab': function (c) {
        if (!c)
            return null;
        var d = Columns['getCurrentRow']();
        delete AllScreenCap[c['id']];
        d['tabs']['push'](c);
        Columns['save']();
        return c;
    },
    'addRow': function (c, d, e) {
        var f = 0x0;
        for (var g in Columns['rowDataMap']) {
            var h = parseInt(g);
            f = f > h ? f : h;
        }
        var j = f + 0x1;
        Columns['rowDataMap'][j] = {
            'id': j,
            'name': e ? c : c + '\x20' + (parseInt(j) - 0x3),
            'tip': d,
            'tabs': []
        };
        localStorage['currentRowId'] = j;
        Columns['save']();
        prepareContextMenu();
        return j;
    },
    'importRow': function (c, d) {
        var e = 0x0;
        for (var f in Columns['rowDataMap']) {
            var g = parseInt(f);
            e = e > g ? e : g;
        }
        var h = e + 0x1;
        var j = {
            'id': h,
            'name': c,
            'tip': d,
            'tabs': []
        };
        Columns['rowDataMap'][h] = j;
        return j;
    },
    'nextRow': function () {
        var c = Columns['getCurrentRowId']();
        var d = Columns['getRowRegistry']();
        var e = 0x0;
        for (var f = 0x0; f < d['length']; f++) {
            if (d[f] == c) {
                e = f + 0x1;
                break;
            }
        }
        localStorage['currentRowId'] = e == d['length'] ? d[0x0] : d[e];
    },
    'prevRow': function () {
        var c = Columns['getCurrentRowId']();
        var d = Columns['getRowRegistry']();
        var e = 0x0;
        for (var f = 0x0; f < d['length']; f++) {
            if (d[f] == c) {
                e = f - 0x1;
                break;
            }
        }
        localStorage['currentRowId'] = e < 0x0 ? d[d['length'] - 0x1] : d[e];
    },
    'removeRow': function () {
        var c = Columns['getCurrentRowId']();
        if (c == 0x0 || c == 0x3)
            return;
        var d = Columns['rowDataMap'][c];
        if (d) {
            Columns['nextRow']();
            delete Columns['rowDataMap'][c];
            Columns['save']();
            var e = menuIdMap[c];
            if (e)
                chrome['contextMenus']['remove'](e);
        }
    },
    'renameRow': function (c, d) {
        var e = Columns['rowDataMap'][c];
        if (e) {
            e['name'] = d;
            Columns['save']();
            var f = menuIdMap[c];
            if (f)
                chrome['contextMenus']['update'](f, { 'title': d });
        }
    },
    'removeTabInRow': function (c) {
        var d = Columns['getCurrentRow']();
        if (d['id'] != 0x3) {
            Columns['addClosedTab'](d['tabs'][c]);
        }
        d['tabs']['splice'](c, 0x1);
        Columns['save']();
    }
};
var hasPermission;
function checkPermission() {
    chrome['permissions']['contains']({ 'origins': ['<all_urls>'] }, function (c) {
        hasPermission = c;
    });
}
function reportNumTabs() {
    if (localStorage['showcount'] == 'false') {
        chrome['browserAction']['setBadgeText']({ 'text': '' });
        return;
    }
    chrome['tabs']['query']({ 'windowType': 'normal' }, function (c) {
        updateBadgeText(c['length']);
    });
}
function updateBadgeText(c) {
    chrome['browserAction']['setBadgeText']({ 'text': '' + c });
    var d = [
        0x0,
        0xff,
        0x0,
        0xff
    ];
    if (c >= 0x2d)
        d = [
            0xff,
            0x0,
            0x0,
            0xff
        ];
    else if (c >= 0x1e)
        d = [
            0xff,
            0x91,
            0x0,
            0xff
        ];
    else if (c >= 0x14)
        d = [
            0xff,
            0xdc,
            0x0,
            0xff
        ];
    else if (c >= 0xa)
        d = [
            0xaf,
            0xe6,
            0x32,
            0xff
        ];
    chrome['browserAction']['setBadgeBackgroundColor']({ 'color': d });
}
function tabData(c) {
    this['id'] = c['id'];
    this['windowId'] = c['windowId'];
    this['title'] = c['title'];
    this['favIconURL'] = c['favIconUrl'];
    this['URL'] = [c['url']];
    this['pinned'] = c['pinned'];
    this['isIncognitoTab'] = c['incognito'];
    this['discarded'] = this['discarded'];
}
var AllScreenCap = {};
function clearCaptures() {
    AllScreenCap = {};
}
function getCapture(c) {
    return AllScreenCap[c['id']];
}
function captureTabPreview(c, d, f) {
    if (!hasPermission) {
        return;
    }
    try {
        if (!f || !f['id'] || !f['windowId'] || f['url'] == '' || f['status'] != 'complete') {
            return;
        }
        if (f['url']['indexOf']('chrome://') == 0x0 || f['url']['indexOf']('chrome-extension://') == 0x0 || f['url']['indexOf']('chrome-devtools://') == 0x0)
            return;
        if (f['active'] || f['selected']) {
            chrome['tabs']['captureVisibleTab'](f['windowId'], {
                'format': 'jpeg',
                'quality': 0x5
            }, function (g) {
                if (g) {
                    AllScreenCap[c] = g;
                }
            });
        }
    } catch (g) {
        console['log']('captureTabPreview', g);
    }
}
function init() {
    reportNumTabs();
    Columns['init']();
    prepareContextMenu();
    prepareBackup();
    checkPermission();
}
function refreshView() {
    var c = chrome['extension']['getViews']({ 'type': 'tab' });
    c['push']['apply'](c, chrome['extension']['getViews']({ 'type': 'popup' }));
    c['forEach'](function (d) {
        if (d['location']['href']['indexOf']('popup.html') > 0x0)
            d['location']['reload']();
    });
}
var menuIdMap = {};
function prepareContextMenu() {
    chrome['contextMenus']['removeAll']();
    menuIdMap = {};
    mainContextId = null;
    if (!localStorage['disableContextMenu'] || localStorage['disableContextMenu'] == 'false') {
        mainContextId = chrome['contextMenus']['create']({
            'title': getMessage('sendToTMT'),
            'contexts': [
                'page',
                'link'
            ],
            'onclick': function (d, e) {
                sendTabDataCM(d, e, 0x0);
            }
        });
        if (localStorage['usageMode'] == '1') {
            var c = Columns['getRowRegistry']();
            if (c['length'] <= 0x2)
                return;
            c['forEach'](function (d) {
                if (d == 0x0 || d > 0x3) {
                    var e = Columns['getRow'](d);
                    if (e) {
                        if (!e['tabs'])
                            e['tabs'] = [];
                        var f = chrome['contextMenus']['create']({
                            'title': e['name'],
                            'contexts': [
                                'page',
                                'link'
                            ],
                            'parentId': mainContextId,
                            'onclick': function (g, h) {
                                for (var j in menuIdMap) {
                                    if (menuIdMap[j] == g['menuItemId']) {
                                        sendTabDataCM(g, h, j);
                                        break;
                                    }
                                }
                            }
                        });
                        menuIdMap[d] = f;
                    }
                }
            });
        }
    }
}
function selectTab(c, d) {
    if (!c || !c['id'] || !c['windowId'])
        return;
    var e = function () {
        chrome['tabs']['update'](c['id'], {
            'active': !![],
            'highlighted': !![]
        }, function (g) {
            if (g['active'] || g['highlighted'])
                closeAllViews();
            else {
                console['error']('cannot\x20focus\x20tab', c['id']);
            }
        });
    };
    var f = c['windowId'] == d;
    if (f) {
        e();
    } else {
        chrome['windows']['update'](c['windowId'], { 'focused': !![] }, function (g) {
            e();
        });
    }
}
function closeAllViews() {
    var c = chrome['extension']['getViews']({ 'type': 'tab' });
    c['push']['apply'](c, chrome['extension']['getViews']({ 'type': 'popup' }));
    c['forEach'](function (d) {
        if (d['location']['href']['indexOf']('popup.html') > 0x0)
            d['close']();
    });
}
function setUsageMode(c, d) {
    if (localStorage['usageMode'] != c || d) {
        localStorage['usageMode'] = c;
        localStorage['currentRowId'] = 0x0;
        prepareContextMenu();
        refreshView();
    }
}
function afterImport(c) {
    Columns['init']();
    if (!c && c != 0x0)
        c = localStorage['usageMode'];
    localStorage['currentRowId'] = 0x0;
    setUsageMode(c, !![]);
}
function getMessage(c, d) {
    try {
        var f = chrome['i18n']['getMessage'](c, d);
        if (f)
            return f;
    } catch (g) {
    }
}
if (chrome['commands'])
    chrome['commands']['onCommand']['addListener'](function (c) {
        switch (c) {
        case 'open-tmt-window':
            chrome['windows']['getCurrent']({}, function (d) {
                openTMTWin(d['id']);
            });
            break;
        case 'send-active-tmt':
            chrome['tabs']['query']({
                'active': !![],
                'currentWindow': !![]
            }, function (d) {
                if (d['length'] == 0x1) {
                    var e = d[0x0]['id'];
                    var f = Columns['getCurrentRowId']();
                    if (f == 0x3) {
                        Columns['nextRow']();
                    }
                    Columns['popTab'](new tabData(d[0x0]));
                    chrome['tabs']['remove'](e);
                }
            });
            break;
        }
    });
var popupWin;
function openTMTWin(c) {
    try {
        localStorage['lastFocusId'] = c;
        var d = chrome['extension']['getViews']({ 'type': 'tab' });
        var f = 0x0;
        if (d['length'] > 0x0) {
            d['forEach'](function (g) {
                if (g['location']['href']['indexOf']('popup.html') > 0x0) {
                    g['location']['reload']();
                    f++;
                    chrome['windows']['update'](popupWin['id'], { 'focused': !![] }, function (h) {
                    });
                }
            });
        }
        if (f == 0x0) {
            chrome['windows']['create']({
                'url': '/core/popup.html?tmtwindow',
                'height': 0x27c,
                'width': parseInt(localStorage['winpopupWidth']) || 0x320,
                'focused': !![],
                'type': 'popup'
            }, function (g) {
                popupWin = g;
            });
        }
    } catch (g) {
        console['error']('opentmtwindow', g);
    }
}
function exportColumns() {
    var c = {};
    var d = Columns['rowDataMap'];
    for (var f in d) {
        if (d[f]['id'] == 0x1 || d[f]['id'] == 0x2)
            continue;
        var g = [];
        var h = d[f]['tabs'];
        if (h['length'] == 0x0) {
            continue;
        }
        for (var i in h) {
            try {
                var j = h[i];
                g['push']({
                    'title': j['title'],
                    'URL': [j['URL'][j['URL']['length'] - 0x1]],
                    'pinned': j['pinned']
                });
            } catch (k) {
                console['error'](k);
            }
        }
        c[f] = {
            'tabs': g,
            'id': d[f]['id'],
            'name': d[f]['name']
        };
    }
    return c;
}
function prepareBackup() {
    var c = exportColumns();
    if (!localStorage['usageMode'])
        localStorage['usageMode'] = 0x0;
    chrome['storage']['local']['set']({
        'rowDataMap': c,
        'usageMode': localStorage['usageMode']
    }, function () {
    });
}
init();