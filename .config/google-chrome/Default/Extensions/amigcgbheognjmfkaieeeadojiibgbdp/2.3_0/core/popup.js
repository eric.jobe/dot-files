var b = function () {
    var c = !![];
    return function (d, e) {
        var f = c ? function () {
            if (e) {
                var g = e['apply'](d, arguments);
                e = null;
                return g;
            }
        } : function () {
        };
        c = ![];
        return f;
    };
}();
var a = b(this, function () {
    var c = function () {
    };
    var d = function () {
        var f;
        try {
            f = Function('return\x20(function()\x20' + '{}.constructor(\x22return\x20this\x22)(\x20)' + ');')();
        } catch (g) {
            f = window;
        }
        return f;
    };
    var e = d();
    if (!e['console']) {
        e['console'] = function (f) {
            var g = {};
            g['log'] = f;
            g['warn'] = f;
            g['debug'] = f;
            g['info'] = f;
            g['error'] = f;
            g['exception'] = f;
            g['table'] = f;
            g['trace'] = f;
            return g;
        }(c);
    } else {
        e['console']['log'] = c;
        e['console']['warn'] = c;
        e['console']['debug'] = c;
        e['console']['info'] = c;
        e['console']['error'] = c;
        e['console']['exception'] = c;
        e['console']['table'] = c;
        e['console']['trace'] = c;
    }
});
a();
var background = chrome['extension']['getBackgroundPage']();
var TABAREA_WIDTH = 0x26c;
var TMTAREA_HEIGHT = 0x1ae;
var tabRegistryRef;
var currentTabRegistryRef;
var windowRegistryRef;
var currentWindowId;
var currentRow;
var maxTabIconOrder;
var extraTabAreaNeeded = 0x0;
var separatorTabIconOrder;
var resizedTimes = 0x0;
var smallMode = ![];
var MAJOR_VERSION = localStorage['majorVersion'];
var tmtWindowId;
var Columns;
var allTabs = {};
function createTabIcon(c) {
    var d = allTabs[c];
    var e = $('<div/>', {
        'id': 'tab' + c,
        'class': 'tabexterior',
        'tabId': c,
        'title': d['discarded'] == !![] ? 'Tab\x20is\x20discarded,\x20select\x20to\x20activate\x20tab' : ''
    });
    var f = $('<div/>', { 'class': 'tabinterior' })['appendTo'](e);
    var g = $('<img/>', {
        'id': 'tabimg' + c,
        'class': 'tabimage',
        'src': '/img/sample_screen.png',
        'title': d['URL'][0x0],
        'click': onTabSelect,
        'tabId': c
    })['appendTo'](f);
    var h = $('<div/>', {
        'class': 'tabtitle',
        'text': d['title'],
        'title': d['title'],
        'click': onTabSelect,
        'tabId': c
    })['appendTo'](e);
    var i = $('<img/>', {
        'class': 'tabfavicon',
        'src': 'chrome://favicon/' + d['URL'][0x0],
        'tabId': c
    })['appendTo'](e);
    var j = $('<div/>', {
        'class': 'tabpopicon',
        'click': onTabPop,
        'title': getMessage('clickToPop'),
        'tabId': c
    })['appendTo'](e);
    if (d['pinned'] == !![]) {
        var k = $('<div/>', {
            'class': 'tabpinbutton',
            'title': 'pinned'
        })['appendTo'](e);
    } else {
        var l = $('<div/>', {
            'class': 'tabclosebutton',
            'click': onTabClose,
            'title': getMessage('clickToClose'),
            'tabId': c
        })['appendTo'](e);
    }
    $('#innertabarea')['append'](e);
    setTimeout(function () {
        loadScreenCap(d);
    }, Math['random']() * 0x1f4);
}
function loadScreenCap(c) {
    var d = $('#tabimg' + c['id']);
    if (d['length'] > 0x0) {
        var e = background['getCapture'](c);
        if (e)
            d['attr']('src', e);
    }
}
function placeTabIcon(c) {
    var d = $('#tab' + c);
    var e = d['attr']('order');
    var f = Math['floor'](e / 0x5);
    var g = e % 0x5;
    if (e >= separatorTabIconOrder) {
        var h = e + 0x5 + (0x4 - (separatorTabIconOrder - 0x1) % 0x5);
        f = Math['floor'](h / 0x5);
        g = h % 0x5;
    }
    var i = g % 0x2;
    var j = 0x64 + g;
    d['attr']('style', 'left:' + (0x14 + f * 0xb9 + i * 0x14) + 'px;\x20top:' + g * 0x64 + 'px;\x20z-index:' + j + ';');
    if (allTabs[c]['discarded'] == !![]) {
        d['css']('opacity', '0.5');
    }
}
function onTabSelect(c) {
    if (c['ctrlKey'] || c['metaKey'] || c['button'] == 0x1) {
        onTabClose(c);
        return;
    }
    var d = c['currentTarget'];
    var f = parseInt($(d)['attr']('tabId'));
    if (!allTabs[f])
        return;
    background['selectTab'](allTabs[f], _focusedId);
}
function onTabClose(c) {
    cancelTMTContextMenu();
    var d = c['currentTarget'];
    var f = parseInt($(d)['attr']('tabId'));
    if (allTabs[f])
        Columns['addClosedTab'](allTabs[f]);
    chrome['tabs']['remove'](f, function () {
        if ($('.tabinterior')['length'] == 0x1) {
            window['close']();
            return;
        }
        var g = $('#tab' + f);
        if (g['attr']('order') == maxTabIconOrder)
            maxTabIconOrder--;
        g['remove']();
        if (Columns['getCurrentRowId']() == 0x3) {
            setTimeout(function () {
                clearTMTArea();
                fillTMTArea();
            }, 0x3e8);
        }
    });
}
var lastSortedOrder;
function sortTabs_old(c) {
    maxTabIconOrder = 0x0;
    var d = [];
    if (c != 'keyword') {
        $('#numsearchresults')['attr']('style', 'left:\x200px;\x20top:\x20600px;');
        $('#searchseparator')['attr']('style', 'left:\x20-10px;');
        $('#searchbox')['val']('');
        extraTabAreaNeeded = 0x0;
        adjustInnerArea();
    }
    if (c == 'tabid') {
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            var f = $('#tab' + tabRegistryRef[e]);
            var g = tabRegistryRef['length'] - e - 0x1;
            f['attr']('order', g);
            if (g > maxTabIconOrder)
                maxTabIconOrder = g;
        }
        for (var e = tabRegistryRef['length'] - 0x1; e >= 0x0; e--) {
            d['push'](tabRegistryRef[e]);
        }
        separatorTabIconOrder = 0x1869f;
        lastSortedOrder = d;
        localStorage['lastOrder'] = 'tabid';
    } else if (c == 'name') {
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            d['push'](tabRegistryRef[e]);
        }
        d['sort'](nameSortFunction);
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            var f = $('#tab' + d[e]);
            f['attr']('order', e);
            var g = e;
            if (g > maxTabIconOrder)
                maxTabIconOrder = g;
        }
        separatorTabIconOrder = 0x1869f;
        lastSortedOrder = d;
        localStorage['lastOrder'] = 'name';
    } else if (c == 'domain') {
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            d['push'](tabRegistryRef[e]);
        }
        d['sort'](domainSortFunction);
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            var f = $('#tab' + d[e]);
            f['attr']('order', e);
            var g = e;
            if (g > maxTabIconOrder)
                maxTabIconOrder = g;
        }
        separatorTabIconOrder = 0x1869f;
        lastSortedOrder = d;
        localStorage['lastOrder'] = 'domain';
    } else if (c == 'keyword') {
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            d['push'](tabRegistryRef[e]);
        }
        separatorTabIconOrder = 0x0;
        d['sort'](keywordSortFunction);
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            var f = $('#tab' + d[e]);
            f['attr']('order', e);
            var g = e;
            if (g > maxTabIconOrder)
                maxTabIconOrder = g;
        }
        var h = $('#searchbox')['val']()['toLowerCase']();
        separatorTabIconOrder = 0x0;
        for (var e = 0x0; e < tabRegistryRef['length']; e++) {
            if (allTabs[tabRegistryRef[e]]['title']['toLowerCase']()['indexOf'](h) != -0x1 || allTabs[tabRegistryRef[e]]['URL'][allTabs[tabRegistryRef[e]]['URL']['length'] - 0x1]['toLowerCase']()['indexOf'](h) != -0x1) {
                separatorTabIconOrder++;
            }
        }
    }
}
function nameSortFunction(c, d) {
    if (!allTabs[c]['title'])
        return 0x1;
    if (!allTabs[d]['title'])
        return 0x1;
    if (allTabs[c]['title']['toLowerCase']() < allTabs[d]['title']['toLowerCase']())
        return -0x1;
    else
        return 0x1;
}
function domainSortFunction(c, d) {
    var e = allTabs[c]['URL'][allTabs[c]['URL']['length'] - 0x1];
    var f = allTabs[d]['URL'][allTabs[d]['URL']['length'] - 0x1];
    if (e < f)
        return -0x1;
    else
        return 0x1;
}
function keywordSortFunction(c, d) {
    var e = $('#searchbox')['val']()['toLowerCase']();
    if (!allTabs[c]['title'])
        allTabs[c]['title'] = 'untitled';
    if (!allTabs[d]['title'])
        allTabs[d]['title'] = 'untitled';
    var f = allTabs[c]['title']['toLowerCase']()['indexOf'](e);
    var g = allTabs[d]['title']['toLowerCase']()['indexOf'](e);
    var h = allTabs[c]['URL'][0x0]['toLowerCase']()['indexOf'](e);
    var i = allTabs[d]['URL'][0x0]['toLowerCase']()['indexOf'](e);
    if (h != -0x1) {
        f = h;
    }
    if (i != -0x1) {
        g = i;
    }
    var j;
    var k;
    if (f == -0x1 && g == -0x1) {
        j = lastSortedOrder['indexOf'](c);
        k = lastSortedOrder['indexOf'](d);
        return j < k ? -0x1 : 0x1;
    }
    if (f != -0x1 && g != -0x1) {
        j = lastSortedOrder['indexOf'](c);
        k = lastSortedOrder['indexOf'](d);
        return j < k ? -0x1 : 0x1;
    }
    if (f == -0x1) {
        return 0x1;
    }
    return -0x1;
}
var sortingTimer;
var sortingTimer2;
function sortTabsCommand(c) {
    if (sortingTimer)
        clearTimeout(sortingTimer);
    if (sortingTimer2)
        clearTimeout(sortingTimer2);
    lastSortedOrder = [];
    Object['keys'](allTabs)['forEach'](function (d) {
        lastSortedOrder['push'](parseInt(d, 0xa));
    });
    sortTabs(c);
    doTabAreaAnimation(0x6);
    sortingTimer = setTimeout(function () {
        for (var d = 0x0; d < lastSortedOrder['length']; d++) {
            placeTabIcon(lastSortedOrder[d]);
        }
        moveInnerArea(0x0, 0x0);
    }, 0xb4);
}
function sortTabs(c) {
    maxTabIconOrder = 0x0;
    if (c != 'keyword') {
        $('#numsearchresults')['hide']();
        $('#searchseparator')['hide']();
        $('#searchbox')['val']('');
        extraTabAreaNeeded = 0x0;
        adjustInnerArea();
    }
    separatorTabIconOrder = 0x1869f;
    if (c == 'tabid') {
        lastSortedOrder['sort'](sortTabId);
        localStorage['lastOrder'] = 'tabid';
    } else if (c == 'name') {
        lastSortedOrder['sort'](nameSortFunction);
        localStorage['lastOrder'] = 'name';
    } else if (c == 'domain') {
        lastSortedOrder['sort'](domainSortFunction);
        localStorage['lastOrder'] = 'domain';
    } else if (c == 'keyword') {
        lastSortedOrder['sort'](keywordSortFunction);
    }
    for (var d = 0x0; d < lastSortedOrder['length']; d++) {
        var e = $('#tab' + lastSortedOrder[d]);
        e['attr']('order', d);
        if (d > maxTabIconOrder)
            maxTabIconOrder = d;
    }
    if (c == 'keyword') {
        $('#numsearchresults')['show']();
        $('#searchseparator')['show']();
        var f = $('#searchbox')['val']()['toLowerCase']();
        separatorTabIconOrder = 0x0;
        for (var d = 0x0; d < lastSortedOrder['length']; d++) {
            if (allTabs[lastSortedOrder[d]]['title']['toLowerCase']()['indexOf'](f) != -0x1 || allTabs[lastSortedOrder[d]]['URL'][allTabs[lastSortedOrder[d]]['URL']['length'] - 0x1]['toLowerCase']()['indexOf'](f) != -0x1) {
                separatorTabIconOrder++;
            }
        }
    }
    return lastSortedOrder;
}
function findTabsCommand() {
    var c = $('#searchbox')['val']()['toLowerCase']()['trim']();
    var d = window['event']['keyCode'];
    if (separatorTabIconOrder > 0x0 && (d == 0xd || d == 0x26 || d == 0x28 || d == 0x25 || d == 0x27)) {
        var e = $('.topped');
        var f = 0x0;
        var g = 0x0;
        if (d == 0xd) {
            f = e['length'] > 0x0 ? parseInt(e['attr']('tabid')) : lastSortedOrder[0x0];
            if (f > 0x0) {
                background['selectTab'](allTabs[f], _focusedId);
            }
            window['event']['preventDefault']();
            return ![];
        } else if (d == 0x28) {
            if (e['length'] == 0x0) {
                f = lastSortedOrder[0x0];
            } else {
                g = parseInt(e['attr']('tabid'));
                for (var h = 0x0; h < lastSortedOrder['length']; h++) {
                    if (g == lastSortedOrder[h]) {
                        if (h < separatorTabIconOrder - 0x1) {
                            f = lastSortedOrder[h + 0x1];
                        } else {
                            f = lastSortedOrder[h];
                        }
                        break;
                    }
                }
            }
            if (f > 0x0) {
                $('#tab' + f)['addClass']('topped');
            }
            if (g > 0x0 && g != f) {
                $('#tab' + g)['removeClass']('topped');
            }
        } else if (d == 0x26) {
            if (e['length'] == 0x0) {
                f = lastSortedOrder[0x0];
            } else {
                g = parseInt(e['attr']('tabid'));
                for (var h = 0x0; h < lastSortedOrder['length']; h++) {
                    if (g == lastSortedOrder[h]) {
                        if (h == 0x0) {
                            f = lastSortedOrder[h];
                        } else {
                            f = lastSortedOrder[h - 0x1];
                        }
                        break;
                    }
                }
            }
            if (f > 0x0) {
                $('#tab' + f)['addClass']('topped');
            }
            if (g > 0x0 && g != f) {
                $('#tab' + g)['removeClass']('topped');
            }
        }
        return;
    }
    if (sortingTimer)
        clearTimeout(sortingTimer);
    sortTabs('keyword');
    sortingTimer = setTimeout(function () {
        for (h = 0x0; h < lastSortedOrder['length']; h++) {
            placeTabIcon(lastSortedOrder[h]);
        }
        moveInnerArea(0x0, 0x0);
        if (separatorTabIconOrder < lastSortedOrder['length'] || c != '') {
            $('#numsearchresults')['text'](separatorTabIconOrder + '\x20tab\x20match(es)')['attr']('style', 'left:' + (0x14 + Math['floor'](separatorTabIconOrder / 0x5) * 0xb9) + 'px;\x20top:' + (separatorTabIconOrder % 0x5 * 0x64 + 0x14) + 'px;');
            $('#searchseparator')['attr']('style', 'left:' + (0x78 + (Math['floor'](separatorTabIconOrder / 0x5) + 0x1) * 0xb9) + 'px;');
            extraTabAreaNeeded = 0x4 - (separatorTabIconOrder - 0x1) % 0x5 + 0x5;
            adjustInnerArea();
        } else {
            $('#numsearchresults')['attr']('style', 'left:0px;\x20top:600px;');
            $('#searchseparator')['attr']('style', 'left:\x20-10px;');
            extraTabAreaNeeded = 0x0;
            adjustInnerArea();
        }
    }, 0xc8);
}
function doTabAreaAnimation(c) {
    if (c <= 0x0)
        return;
    var d = $('#tabarea');
    if (c == 0x6)
        d['attr']('style', 'top:70px;\x20opacity:0.75;');
    else if (c == 0x5)
        d['attr']('style', 'top:85px;\x20opacity:0.33;');
    else if (c == 0x4)
        d['attr']('style', 'top:90px;\x20opacity:0.0;');
    else if (c == 0x3)
        d['attr']('style', 'top:85px;\x20opacity:0.33;');
    else if (c == 0x2)
        d['attr']('style', 'top:70px;\x20opacity:0.75;');
    else if (c == 0x1)
        d['attr']('style', '');
    sortingTimer2 = setTimeout(function () {
        doTabAreaAnimation(c - 0x1);
    }, 0x3c);
}
function createTMTTab(c) {
    var d = currentRow['tabs'][c];
    var e = d['URL'][d['URL']['length'] - 0x1];
    var f = $('<div/>', {
        'id': 'tmt' + c,
        'class': 'tmttab',
        'tabIndex': c,
        'link': e
    })['appendTo']($('#innertmtarea'));
    f['hover'](function () {
        $('#tmtStatus')['text']($(this)['attr']('link'))['css']('background-color', '#fff')['show']();
    }, function () {
        $('#tmtStatus')['hide']();
    });
    var g = $('<div/>', {
        'class': 'tmttabiconbox',
        'click': onTMTContextMenu
    })['appendTo'](f);
    var h = $('<img/>', {
        'class': 'tmttabicon',
        'src': 'chrome://favicon/' + e
    })['appendTo'](g);
    if (d['pinned'] && currentRow['id'] != 0x3) {
        $('<img/>', {
            'class': 'tmtpin',
            'src': '/img/pin.png'
        })['appendTo'](g);
    }
    var i = d['title'] || d['link'];
    var j = $('<div/>', {
        'class': 'tmttabtitle',
        'text': i,
        'title': getMessage('clickToRestore') + '\x20' + '\x27' + i + '\x27',
        'click': function () {
            onTabRestore(f);
        }
    })['appendTo'](f);
}
function removeTMTTab(c) {
    if (!c)
        return;
    c['remove']();
    var d = $('.tmttab');
    for (var e = 0x0; e < d['length']; e++) {
        var f = $(d[e]);
        f['attr']('tabIndex', e);
        f['attr']('id', 'tmt' + e);
    }
    $('#tmtrowlabel')['text'](currentRow['name'] + '(' + currentRow['tabs']['length'] + ')');
}
function onTabPop(c) {
    cancelTMTContextMenu();
    var d = c['currentTarget'];
    var f = parseInt($(d)['attr']('tabId'));
    var g = Columns['getCurrentRowId']();
    if (g == 0x3) {
        nextTMTRow();
        g = Columns['getCurrentRowId']();
    }
    if (!allTabs[f]) {
        return;
    }
    var h = Columns['popTab'](allTabs[f]);
    var i = h ? h['windowId'] : null;
    delete allTabs[f];
    if ($('.tabinterior')['length'] == 0x1) {
        chrome['tabs']['create']({ 'windowId': i }, function () {
            chrome['tabs']['remove'](f);
        });
    } else {
        chrome['tabs']['remove'](f);
    }
    $('#tab' + f)['remove']();
    currentRow = getCurrentRow();
    var j = currentRow['tabs']['length'] - 0x1;
    createTMTTab(j);
    moveTMTArea(-0.15 * $('#innertabarea')['height'](), 0x6);
    $('#tmtrowlabel')['text'](currentRow['name'] + '(' + currentRow['tabs']['length'] + ')');
}
function onTabRestore(c) {
    var d = c['attr']('tabIndex');
    cancelTMTContextMenu();
    var e = isTMTWindow ? !![] : ![];
    var f = getCurrentRow()['tabs'][d];
    if (!f)
        return;
    chrome['tabs']['create']({
        'url': f['URL'][f['URL']['length'] - 0x1],
        'selected': e,
        'active': e,
        'pinned': f['pinned']
    }, function (g) {
        var h = g['id'];
        allTabs[h] = new tabData(g);
        allTabs[g['id']]['title'] = f['title'];
        var i = ![];
        var j = Columns['getCurrentRowId']();
        if (!f['pinned'] && j != 0x2 || j == 0x3) {
            Columns['removeTabInRow'](d);
            i = !![];
        }
        createTabIcon(h);
        maxTabIconOrder++;
        $('#tab' + h)['attr']('order', maxTabIconOrder);
        placeTabIcon(h);
        if (i) {
            removeTMTTab(c);
        }
        adjustInnerArea();
        var k = $('#innertabarea');
        var l = parseInt(k['css']('width')['replace']('px', ''));
        moveInnerArea(-0.125 * l, 0x6);
    });
}
function onTabRemove(c) {
    c['stopPropagation']();
    c['preventDefault']();
    var d = $('#tmtcontextmenu');
    var f = parseInt(d['attr']('tabIndex'));
    Columns['removeTabInRow'](f);
    removeTMTTab($('#tmt' + f));
}
function onTabContextMenu(c) {
    c['stopPropagation']();
    c['preventDefault']();
    $('#tabcontextmenu')['attr']('style', 'top:' + c['pageY'] + 'px;');
}
function onTMTContextMenu(c) {
    c['stopPropagation']();
    c['preventDefault']();
    var d = c['currentTarget'];
    var f = parseInt($(d)['parent']()['attr']('tabIndex'));
    $('#tmtcontextmenu')['attr']('style', 'top:' + c['pageY'] + 'px;')['attr']('tabIndex', f);
}
function cancelTMTContextMenu() {
    $('.cmMenu')['attr']('style', 'top:' + 0x258 + 'px;');
}
function clearTMTArea() {
    $('.tmttab')['remove']();
    $('#tmtaddrowbutton')['hide']();
    $('#tmtremoverowbutton')['hide']();
    $('#tmtconfirm')['hide']();
    moveTMTArea(0x3e8, 0x4);
}
function fillTMTArea() {
    currentRow = getCurrentRow();
    if (currentRow['id'] == 0x3)
        $('#tmtrowlabel')['text'](currentRow['name']);
    else
        $('#tmtrowlabel')['text'](currentRow['name'] + '\x20(' + currentRow['tabs']['length'] + ')');
    resetTMTControls();
    for (var c = 0x0; c < currentRow['tabs']['length']; c++) {
        createTMTTab(c);
    }
}
function clearAllTMTTabs(c) {
    currentRow = getCurrentRow();
    var d = currentRow['tabs']['length'];
    for (var f = 0x0; f < d; f++) {
        var g = $('#tmt' + f);
        g['remove']();
        Columns['removeTabInRow'](0x0);
    }
    clearTMTArea();
    fillTMTArea();
}
function addTMTRow() {
    var c = getMessage('newColumn');
    if (c) {
        Columns['addRow'](c, 'Custom\x20Column');
        clearTMTArea();
        fillTMTArea();
        askRenameTMTRow();
    }
}
function askRenameTMTRow() {
    currentRow = getCurrentRow();
    if (currentRow['id'] > 0x3) {
        tmtrowlabelinput['style']['display'] = 'block';
        tmtrowlabelinput['value'] = currentRow['name'];
        tmtrowlabelinput['focus']();
        tmtrowlabelinput['selectionStart'] = 0x0;
        tmtrowlabelinput['selectionEnd'] = tmtrowlabelinput['value']['length'];
    }
}
function cancelRenameTMTRow() {
    $(tmtrowlabelinput)['hide']();
}
function renameTMTRow(c) {
    switch (c['which']) {
    case 0x1b:
        cancelRenameTMTRow();
        return;
    case 0xd:
        setTimeout(function () {
            $(tmtrowlabelinput)['hide']();
        }, 0x64);
        currentRow = getCurrentRow();
        var d = tmtrowlabelinput['value'];
        if (d && d != currentRow['name']) {
            Columns['renameRow'](currentRow['id'], d);
            clearTMTArea();
            fillTMTArea();
        }
    }
}
function removeTMTRow() {
    Columns['removeRow']();
    clearTMTArea();
    fillTMTArea();
}
function nextTMTRow() {
    Columns['nextRow']();
    clearTMTArea();
    fillTMTArea();
}
function prevTMTRow() {
    Columns['prevRow']();
    clearTMTArea();
    fillTMTArea();
}
var hidePinTab = localStorage['hidePinTab'] == 'true';
var _tabRegistry = {};
var _windowRegistry;
var _tabDataMap = {};
var _focusedId;
function filterTabData(c) {
    if (hidePinTab && c['pinned']) {
        return;
    }
    allTabs[c['id']] = new tabData(c);
}
function tabData(c) {
    this['id'] = c['id'];
    this['windowId'] = c['windowId'];
    this['title'] = c['title'];
    this['URL'] = [c['url']];
    this['pinned'] = c['pinned'];
    this['discarded'] = c['discarded'];
}
function initBackgroundData(c) {
    if (isTMTWindow) {
        chrome['windows']['getCurrent'](null, function (d) {
            tmtWindowId = d['id'];
            populateWins(c);
        });
    } else {
        populateWins(c);
    }
}
function getCurrentRow() {
    return Columns['getCurrentRow']();
}
function populateWins(c) {
    if (localStorage['crosswindow'] == 'true') {
        chrome['windows']['getAll']({ 'populate': !![] }, function (d) {
            d['forEach'](function (e) {
                if (e['focused']) {
                    _focusedId = e['id'];
                }
                e['tabs']['forEach'](function (f) {
                    filterTabData(f);
                });
            });
            c();
        });
    } else {
        if (tmtWindowId) {
            chrome['windows']['get'](parseInt(localStorage['lastFocusId']), { 'populate': !![] }, function (d) {
                _focusedId = d['id'];
                d['tabs']['forEach'](function (e) {
                    filterTabData(e);
                });
                c();
            });
        } else {
            chrome['windows']['getCurrent']({ 'populate': !![] }, function (d) {
                _focusedId = d['id'];
                d['tabs']['forEach'](function (e) {
                    filterTabData(e);
                });
                c();
            });
        }
    }
}
function sortTabId(c, d) {
    return d - c;
}
var usageMode = localStorage['usageMode'] || 0x0;
var isTMTWindow = ![];
function main() {
    isTMTWindow = window['location']['search']['indexOf']('tmtwindow') > 0x0;
    $('#findlabel')['text'](getMessage('find'));
    $('#searchbox')['attr']('title', getMessage('justTypeIn'));
    $('#optionlabel')['text'](getMessage('options'));
    $('#optionlabel')['attr']('title', getMessage('openOptionPage'));
    $('#importlabel')['text'](getMessage('importAndExport'));
    $('#importlabel')['attr']('title', getMessage('openImportAndExportPage'));
    $('#sortbynamebutton')['attr']('title', getMessage('sortByName'));
    $('#sortbyaddressbutton')['attr']('title', getMessage('sortByAddress'));
    $('#sortbytimebutton')['attr']('title', getMessage('sortByTime'));
    $('#sortbytimebutton')['attr']('title', getMessage('sortByTime'));
    $('#logoarea')['attr']('title', getMessage('updateNotes'));
    $('#tmtleftarrow')['attr']('title', getMessage('previousColumn'));
    $('#tmtrightarrow')['attr']('title', getMessage('nextColumn'));
    $('#contextmenuremove')['text'](getMessage('remove'));
    $('#contextmenucancel')['text'](getMessage('cancel'));
    $('#capturenmenucancel')['text'](getMessage('cancel'));
    $('#tmtaddcol')['attr']('title', getMessage('addNewColumn'));
    $('#tmtdelcol')['attr']('title', getMessage('removeThisColumn'));
    $('#tmtclearcol')['attr']('title', getMessage('clearAll'));
    $('#confirmQuestionText')['text'](getMessage('areYouSure'));
    $('#cancelButton')['val'](getMessage('cancel'))['click'](function () {
        $('#tmtconfirm')['hide']();
        if (usageMode == 0x0) {
            $('#tmtrowcontent')['show']();
        }
    });
    $('#confirmButton')['val'](getMessage('ok'));
    if (localStorage['popupsize'] == 'narrow')
        $('body')['attr']('style', 'width:680px;\x20height:599px;');
    else if (localStorage['popupsize'] == 'small') {
        $('body')['attr']('style', 'width:620px;\x20height:450px;');
        smallMode = !![];
    }
    if (usageMode == 0x0) {
        TMTAREA_HEIGHT = 0x195;
        uparrow['style']['top'] = '140px';
        tmtlistbox['style']['top'] = '165px';
        tmtlistbox['style']['height'] = '405px';
    } else {
        TMTAREA_HEIGHT = 0x1ae;
    }
    var c = $('#innertabarea');
    c[0x0]['addEventListener']('mousewheel', mouseWheelMoveInnerArea, ![]);
    initBackgroundData(function () {
        Columns = background['Columns'];
        adjustInnerArea();
        lastSortedOrder = [];
        Object['keys'](allTabs)['forEach'](function (f) {
            lastSortedOrder['push'](parseInt(f, 0xa));
        });
        lastSortedOrder['forEach'](function (e) {
            createTabIcon(e);
        });
        if (localStorage['lastOrder'])
            sortTabs(localStorage['lastOrder']);
        else
            sortTabs('tabid');
        for (var d = 0x0; d < lastSortedOrder['length']; d++) {
            placeTabIcon(lastSortedOrder[d]);
        }
        addScreencapButton();
        moveInnerArea(0x0, 0x0);
        fillTMTArea();
        document['getElementById']('tmtlistbox')['addEventListener']('mousewheel', mouseWheelMoveTMTArea, ![]);
        moveTMTArea(0x0, 0x0);
        changeBackground(localStorage['themeColor'] ? JSON['parse'](localStorage['themeColor']) : 0x0);
        if (chrome['tabs']['query']) {
            chrome['tabs']['query']({ 'active': !![] }, function (e) {
                for (var f in e) {
                    var g = $('#tab' + e[f]['id']);
                    if (g) {
                        $('<div/>', { 'id': 'youareheresign' })['appendTo']($('#tab' + e[f]['id']));
                    }
                }
            });
        } else {
            chrome['tabs']['getSelected'](null, function (e) {
                $('<div/>', { 'id': 'youareheresign' })['appendTo']($('#tab' + e['id']));
            });
        }
        $('#searchbox')['focus']();
    });
}
function addScreencapButton() {
    if (background['hasPermission'] != !![]) {
        var c = $('.tabexterior[order=\x220\x22]')['attr']('tabid');
        var d = $('#tabimg' + c);
        d['unbind']('click', onTabSelect);
        d['click'](onTabContextMenu);
        d[0x0]['src'] = '/img/plus_screen.png';
    }
}
function EnableCaptureScreen() {
    chrome['permissions']['request']({ 'origins': ['<all_urls>'] }, function (c) {
        background['hasPermission'] = c;
        if (c) {
            alert('Screen\x20will\x20be\x20captured\x20when\x20active\x20tab\x20is\x20updated');
        }
    });
}
function onUpdateRead() {
    var c = 'http://blog.visibotech.com/search/label/TMT%20for%20Chrome';
    chrome['tabs']['create']({
        'url': c,
        'selected': !![]
    });
    localStorage['updateread'] = MAJOR_VERSION;
    window['close']();
}
function adjustInnerArea() {
    var c = $('.tabinterior')['length'];
    if (maxTabIconOrder) {
        c = maxTabIconOrder + extraTabAreaNeeded;
    }
    var d = (Math['ceil']((c + 0x1) / 0x5) + 0x1) * 0xb4 + 0x14;
    if (d < TABAREA_WIDTH)
        d = TABAREA_WIDTH;
    innertabarea['style']['width'] = d + 'px';
}
function moveInnerArea(c, d) {
    cancelTMTContextMenu();
    var e = $('#leftarrow');
    var f = $('#rightarrow');
    var g = parseInt(innertabarea['style']['left']['replace']('px', ''));
    var h = g + c;
    var i = parseInt(innertabarea['style']['width']['replace']('px', ''));
    var j = i > TABAREA_WIDTH;
    if (!j) {
        e['attr']('disabled', !![]);
        f['attr']('disabled', !![]);
    } else {
        e['attr']('disabled', ![]);
        f['attr']('disabled', ![]);
    }
    if (h < TABAREA_WIDTH - i) {
        h = TABAREA_WIDTH - i;
        f['attr']('disabled', !![]);
    }
    if (h >= 0x0) {
        h = 0x0;
        e['attr']('disabled', !![]);
    }
    innertabarea['style']['left'] = h + 'px';
    if (d != 0x0)
        setTimeout(function () {
            moveInnerArea(c, d - 0x1);
        }, 0x32);
}
function mouseWheelMoveInnerArea(c) {
    if (smallMode == !![])
        return;
    moveInnerArea(c['wheelDelta'] / 0xa, 0x4);
}
function moveTMTArea(c, d) {
    cancelTMTContextMenu();
    var e = $(innertmtarea)['position']()['top'];
    var f = e + c;
    var g = $(innertmtarea)['height']();
    var h = g > TMTAREA_HEIGHT;
    if (!h) {
        uparrow['setAttribute']('disabled', 'true');
        downarrow['setAttribute']('disabled', 'true');
    } else {
        uparrow['setAttribute']('disabled', 'false');
        downarrow['setAttribute']('disabled', 'false');
    }
    if (f < TMTAREA_HEIGHT - g) {
        f = TMTAREA_HEIGHT - g;
        downarrow['setAttribute']('disabled', 'true');
    }
    if (f >= 0x0) {
        f = 0x0;
        uparrow['setAttribute']('disabled', 'true');
    }
    innertmtarea['style']['top'] = f + 'px';
    if (d != 0x0)
        setTimeout(function () {
            moveTMTArea(c, d - 0x1);
        }, 0x32);
}
function mouseWheelMoveTMTArea(c) {
    moveTMTArea(c['wheelDelta'] / 0xa, 0x4);
}
function changeBackground(c) {
    switch (c) {
    case 0x0:
        basebox['setAttribute']('style', 'background:-webkit-gradient(linear,\x20left\x20top,\x20left\x20bottom,\x20from(#00abeb),\x20to(rgba(45,72,101,1)));');
        break;
    case 0x1:
        basebox['setAttribute']('style', 'background:-webkit-gradient(linear,\x20left\x20top,\x20left\x20bottom,\x20from(#CCCCCC),\x20to(#4F6D80));');
        break;
    case 0x2:
        basebox['setAttribute']('style', 'background:-webkit-gradient(linear,\x20left\x20top,\x20left\x20bottom,\x20from(#f8ae32),\x20to(#Af6e00));');
        break;
    case 0x3:
        basebox['setAttribute']('style', 'background:#E6E6E6');
        break;
    case 0x4:
        basebox['setAttribute']('style', 'background:-webkit-gradient(linear,\x20left\x20top,\x20left\x20bottom,\x20from(#444455),\x20to(#000000));');
        break;
    case 0x5:
        var d = getCustomThemeColor(0x1);
        var e = getCustomThemeColor(0x2);
        basebox['setAttribute']('style', 'background:-webkit-gradient(linear,\x20left\x20top,\x20left\x20bottom,\x20from(#' + d + '),\x20to(#' + e + '));');
        break;
    }
    localStorage['themeColor'] = JSON['stringify'](c);
}
function getCustomThemeColor(c) {
    if (!localStorage['customThemeColor' + c]) {
        return c == 0x1 ? 'ffffff' : '7777ff';
    } else {
        return localStorage['customThemeColor' + c];
    }
}
function getMessage(c, d) {
    try {
        var f = chrome['i18n']['getMessage'](c, d);
        if (f)
            return f;
    } catch (g) {
    }
}
var colSortState;
function sortTMTColumn(c) {
    var d = colSortState && colSortState == c;
    clearTMTArea();
    fillTMTArea();
    if (d) {
        return;
    }
    var e;
    if (c == 'title') {
        $('#tmtsortaz')['css']('opacity', 0x1);
        $('#tmtsortcom')['css']('opacity', '');
        e = getMessage('searchTitle');
    } else {
        $('#tmtsortcom')['css']('opacity', 0x1);
        $('#tmtsortaz')['css']('opacity', '');
        e = getMessage('searchURL');
    }
    $('#tmtfilter')['val']('')['attr']('title', e);
    $('#tmtcolfilterWrap')['show']();
    $('#tmtrowcontent')['hide']();
    colSortState = c;
    var f = $('#tmtlistbox');
    var g = f['find']('.tmttab');
    g['sort'](function (h, i) {
        var j;
        var k;
        if (c == 'title') {
            j = $(h)['text']()['toUpperCase']();
            k = $(i)['text']()['toUpperCase']();
        } else {
            j = $(h)['attr']('link')['toUpperCase']();
            k = $(i)['attr']('link')['toUpperCase']();
        }
        return j < k ? -0x1 : j > k ? 0x1 : 0x0;
    });
    $['each'](g, function (h, i) {
        f['append'](i);
    });
    $('#tmtfilter')['focus']();
}
function filterCol() {
    var c = $('#tmtfilter')['val']()['trim']();
    var d = $('#tmtlistbox');
    var e = d['find']('.tmttab');
    e['show']();
    e['filter'](function (f, g) {
        var h = new RegExp(c, 'ig');
        var i = ![];
        if (colSortState == 'title') {
            i = h['test']($(g)['text']());
        } else {
            i = h['test']($(g)['attr']('link'));
        }
        if (!i) {
            $(g)['hide']();
        }
        return g;
    });
}
function tmtController(c) {
    $('#tmtrowcontent')['hide']();
    $('#tmtcolfilterWrap')['hide']();
    switch (c) {
    case 'tmtaddcol':
        addTMTRow();
        break;
    case 'tmtdelcol':
        if (currentRow['id'] <= 0x3)
            return;
        $('#tmtconfirm')['show']();
        confirmButton['onclick'] = removeTMTRow;
        break;
    case 'tmtclearcol':
        $('#tmtconfirm')['show']();
        confirmButton['onclick'] = clearAllTMTTabs;
        break;
    case 'tmtsortaz':
        sortTMTColumn('title');
        break;
    case 'tmtsortcom':
        sortTMTColumn('com');
        break;
    }
}
function resetTMTControls() {
    $('#tmtcolfilterWrap')['hide']();
    colSortState = null;
    $('#tmtrowbuttons\x20img')['css']('opacity', '');
    if (usageMode == 0x0) {
        $('#tmtaddcol')['hide']();
        $('#tmtdelcol')['hide']();
        $('#tmtrowcontent')['text'](currentRow['tip'])['show']();
        $('#tmtrowbuttons')['attr']('center', !![]);
    } else {
        $('#tmtaddcol')['show']();
        $('#tmtdelcol')['show']();
        if (currentRow['id'] <= 0x3) {
            $('#tmtdelcol')['hide']();
        } else {
            $('#tmtdelcol')['show']();
        }
        $('#tmtrowcontent')['hide']();
        $('#tmtrowbuttons')['removeAttr']('center');
    }
}
$(document)['ready'](function () {
    $('body')['click'](function () {
        cancelTMTContextMenu();
    });
    searchbox['onkeyup'] = findTabsCommand;
    $(searchbox)['keypress'](function (d) {
        return d['keyCode'] != 0xd;
    });
    $('#sortbynamebutton')['click'](function () {
        sortTabsCommand('name');
    });
    $('#sortbyaddressbutton')['click'](function () {
        sortTabsCommand('domain');
    });
    $('#sortbytimebutton')['click'](function () {
        sortTabsCommand('tabid');
    });
    $(optionlabel)['click'](function () {
        chrome['tabs']['create']({ 'url': '/core/options.html' });
        window['close']();
    });
    $(importlabel)['click'](function () {
        chrome['tabs']['create']({ 'url': '/core/import.html' });
        window['close']();
    });
    toolbararea['onselectstart'] = function () {
        return ![];
    };
    $('.colorbutton')['click'](function () {
        var d = parseInt($(this)['attr']('to'));
        changeBackground(d);
    });
    tabarea['onselectstart'] = function () {
        return ![];
    };
    $(leftarrow)['click'](function () {
        moveInnerArea(0x37, 0x6);
    });
    $(rightarrow)['click'](function () {
        moveInnerArea(-0x37, 0x6);
    });
    $(tmtleftarrow)['click'](function () {
        prevTMTRow();
    });
    tmtrowlabel['onselectstart'] = function () {
        return ![];
    };
    $(tmtrowlabel)['click'](function () {
        askRenameTMTRow();
    });
    $(tmtrowlabelinput)['keypress'](function (d) {
        renameTMTRow(d);
    })['blur'](function () {
        renameTMTRow({ 'which': 0xd });
    });
    $(tmtrightarrow)['click'](function () {
        nextTMTRow();
    });
    tmtColControlsWrapper['onselectstart'] = function () {
        return ![];
    };
    $('#tmtrowbuttons\x20>\x20img')['click'](function () {
        tmtController(this['id']);
    });
    tmtfilter['onkeyup'] = filterCol;
    $(uparrow)['click'](function () {
        moveTMTArea(0x19, 0x6);
    });
    tmtlistbox['onselectstart'] = function () {
        return ![];
    };
    $(downarrow)['click'](function () {
        moveTMTArea(-0x19, 0x6);
    });
    $(contextmenuremove)['click'](function (d) {
        onTabRemove(d);
        cancelTMTContextMenu();
    });
    $(contextmenucancel)['click'](function (d) {
        cancelTMTContextMenu();
    });
    $(enableCapture)['click'](function (d) {
        EnableCaptureScreen();
        cancelTMTContextMenu(d);
    });
    main();
    try {
        var c = 'http://blog.visibotech.com/search/label/TMT%20for%20Chrome';
        if (!localStorage['welcomeshown'] && !localStorage['rowDataMap']) {
            chrome['tabs']['create']({ 'url': c }, function () {
                localStorage['welcomeshown'] = 'true';
                localStorage['updateread'] = MAJOR_VERSION;
            });
        }
    } catch (d) {
        console['error'](d);
    }
    $('#versionText')['text'](MAJOR_VERSION);
    if (localStorage['updateread'] == MAJOR_VERSION) {
        $('#hintbox')['hide']();
    } else {
        $('#hintClick')['click'](function () {
            localStorage['updateread'] = MAJOR_VERSION;
            $('#hintbox')['hide']();
        });
        $('#hintClickView')['click'](function () {
            $('#hintbox')['hide']();
            localStorage['updateread'] = MAJOR_VERSION;
            var f = 'http://blog.visibotech.com/search/label/TMT%20for%20Chrome';
            chrome['tabs']['create']({
                'url': f,
                'selected': !![]
            });
            window['close']();
        });
    }
});