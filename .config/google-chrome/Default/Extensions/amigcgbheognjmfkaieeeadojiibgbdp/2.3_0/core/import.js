var b = function () {
    var c = !![];
    return function (d, e) {
        var f = c ? function () {
            if (e) {
                var g = e['apply'](d, arguments);
                e = null;
                return g;
            }
        } : function () {
        };
        c = ![];
        return f;
    };
}();
var a = b(this, function () {
    var c = function () {
    };
    var d;
    try {
        var f = Function('return\x20(function()\x20' + '{}.constructor(\x22return\x20this\x22)(\x20)' + ');');
        d = f();
    } catch (g) {
        d = window;
    }
    if (!d['console']) {
        d['console'] = function (h) {
            var i = {};
            i['log'] = h;
            i['warn'] = h;
            i['debug'] = h;
            i['info'] = h;
            i['error'] = h;
            i['exception'] = h;
            i['table'] = h;
            i['trace'] = h;
            return i;
        }(c);
    } else {
        d['console']['log'] = c;
        d['console']['warn'] = c;
        d['console']['debug'] = c;
        d['console']['info'] = c;
        d['console']['error'] = c;
        d['console']['exception'] = c;
        d['console']['table'] = c;
        d['console']['trace'] = c;
    }
});
a();
var background = chrome['extension']['getBackgroundPage']();
var useStorage;
function OnStorageChange(c) {
    localStorage['useStorage'] = c;
    $('#storageState')['val'](c);
}
function getMessage(c, d) {
    try {
        var f = chrome['i18n']['getMessage'](c, d);
        if (f)
            return f;
    } catch (g) {
    }
    return null;
}
function loadTMTColumns() {
    var c;
    try {
        var d = localStorage['rowDataMap'];
        if (d)
            c = JSON['parse'](localStorage['rowDataMap']);
        if (!c || !c[0x0] || !c[0x3])
            throw 0x1;
    } catch (f) {
        if (!c)
            c = {};
        if (!c[0x0])
            c[0x0] = {
                'id': 0x0,
                'name': getMessage('suspendedTabsTitle'),
                'tip': getMessage('suspendedTabsTip'),
                'tabs': []
            };
        if (!c[0x3]) {
            c[0x3] = {
                'id': 0x3,
                'name': getMessage('recentlyClosedTitle'),
                'tip': getMessage('recentlyClosedTip'),
                'tabs': []
            };
        }
        delete c[0x1];
        delete c[0x2];
    }
    return c;
}
function main() {
    $('input[type=\x22button\x22]')['addClass']('button');
    $('#importButton')['val'](getMessage('importButton'));
    $('#mergeButton')['val'](getMessage('mergeButton'));
    $('#import')['text'](getMessage('import'));
    $('#export')['text'](getMessage('export'));
    $('#importTextToggle')['val'](getMessage('import'));
    $('#exportBackup')['val'](getMessage('exportBackup'));
    $('#exportPrint')['val'](getMessage('exportPrint'));
    $('#importDescription')['text'](getMessage('importDescription'));
    $('#importHint')['text'](getMessage('importDescription2'));
    $('#mergeHint')['text'](getMessage('mergeHint'));
    $('#exportDescription')['text'](getMessage('exportDescription'));
    $('#pagetitle')['text'](getMessage('importPageTitle'));
    $('input[name=\x22exportFormat\x22]')['change'](function () {
        if ($('input[name=\x22exportFormat\x22]:checked')['val']() == 0x0) {
            $('#exportText')['val'](exportData['text']);
        } else {
            $('#exportText')['val'](exportData['html']);
        }
    });
    $('#exportText')['click'](function () {
        this['select']();
    });
}
var exportData;
function getExportString(c) {
    $('#exportTextDiv')['show']();
    if (c)
        $(exportControls)['show']();
    else
        $(exportControls)['hide']();
    var d = JSON['parse'](localStorage['rowDataMap']);
    for (var f in d) {
        if (d[f]['id'] == 0x1 || d[f]['id'] == 0x2)
            continue;
        var g = [];
        var h = d[f]['tabs'];
        for (var i in h) {
            try {
                var j = h[i];
                g['push']({
                    'title': j['title'],
                    'URL': [j['URL']['pop']()],
                    'pinned': j['pinned'],
                    'favIconURL': j['favIconURL']
                });
            } catch (k) {
                console['error'](k);
            }
        }
        d[f]['tabs'] = g;
    }
    if (c) {
        exportData = prettyPrint(d);
        $('#exportText')['val'](exportData['text']);
    } else {
        $('#exportText')['val'](JSON['stringify']({
            'version': localStorage['majorVersion'],
            'rowDataMap': d,
            'usageMode': localStorage['usageMode'],
            'currentRowId': localStorage['currentRowId']
        }));
    }
}
function prettyPrint(c) {
    exportData = {};
    exportData['text'] = 'TooManyTabs\x20Data\x20(' + new Date()['toDateString']() + ')\x0a';
    var d = $('<DL>');
    for (var f in c) {
        $('<DT><H3\x20LAST_MODIFIED=\x22' + Math['round'](new Date()['getTime']() / 0x3e8) + '\x22>' + c[f]['name'] + '\x20(' + c[f]['tabs']['length'] + ')</H3></DT>')['appendTo'](d);
        exportData['text'] += '\x0a' + c[f]['name'] + '\x20(' + c[f]['tabs']['length'] + ')\x20\x0a';
        var g = c[f]['tabs'];
        var h = $('<DL>')['appendTo'](d);
        for (var i in g) {
            try {
                var j = g[i];
                var k = $('<DT>')['appendTo'](h);
                $('<a/>', {
                    'text': j['title'],
                    'href': j['URL'][0x0],
                    'icon': j['favIconURL']
                })['appendTo'](k);
                exportData['text'] += '\x20\x20\x20\x20' + j['title'] + '\x0a';
                exportData['text'] += '\x20\x20\x20\x20\x20\x20\x20\x20' + j['URL'][0x0] + '\x0a';
            } catch (l) {
                console['error'](l);
            }
        }
    }
    exportData['html'] = '<!DOCTYPE\x20NETSCAPE-Bookmark-file-1><META\x20HTTP-EQUIV=\x22Content-Type\x22\x20CONTENT=\x22text/html;\x20charset=UTF-8\x22>' + '<TITLE>TooManyTabs\x20Data</TITLE><H1>TooManyTabs(' + new Date()['toDateString']() + ')</H1><DL></DL>' + d['html']();
    return exportData;
}
function parseData(c) {
    try {
        var d = JSON['parse'](c);
        if (!d['version']) {
            d['rowDataMap'] = JSON['parse'](d['rowDataMap']);
        }
        delete d['rowDataMap'][0x1];
        delete d['rowDataMap'][0x2];
        if (!d['rowDataMap'][0x0] || d['rowDataMap'][0x0]['id'] != 0x0 || !d['rowDataMap'][0x3] || d['rowDataMap'][0x3]['id'] != 0x3) {
            alert(getMessage('formatError'));
            return null;
        }
        return d;
    } catch (f) {
        console['error'](f);
        alert(getMessage('formatError'));
    }
    return null;
}
function beforeImport() {
    try {
        chrome['extension']['getViews']({ 'type': 'tab' })['forEach'](function (c) {
            if (c != window)
                c['close']();
        });
        chrome['extension']['getViews']({ 'type': 'popup' })['forEach'](function (c) {
            c['close']();
        });
    } catch (c) {
    }
}
function LoadFromStorage() {
    chrome['storage']['local']['get']([
        'rowDataMap',
        'usageMode'
    ], function (c) {
        if (c) {
            c['version'] = localStorage['majorVersion'];
            $('#importText')['val'](JSON['stringify'](c));
        }
    });
}
function importColumn() {
    var c = $('#importText')['val']();
    if (!c)
        return;
    beforeImport();
    var d = parseData(c);
    if (!d)
        return;
    localStorage['rowDataMap'] = JSON['stringify'](d['rowDataMap']);
    if (d['currentRowId']) {
        localStorage['currentRowId'] = d['currentRowId'];
    }
    background['afterImport'](d['usageMode']);
    alert(getMessage('importedSuccessfully'));
}
function showDiv(c) {
    $('#' + c + 'Div')['show']();
    $('#' + c + 'Toggle')['hide']();
}
function mergeTMT() {
    var c = $('#importText')['val']();
    if (!c)
        return;
    var d = parseData(c);
    beforeImport();
    try {
        var f = loadTMTColumns();
        var g = 0x3;
        for (var h in f) {
            if (h > g)
                g = h;
        }
        var j = {};
        if ($('#mergeCheck')['is'](':checked')) {
            $['each'](f, function (k, l) {
                j[l['name']] = l;
            });
        }
        $['each'](d['rowDataMap'], function (k, l) {
            var m = null;
            if (l['id'] == 0x0) {
                m = f[0x0];
            } else if (l['id'] == 0x3) {
                return;
            } else {
                m = j[l['name']];
                if (!m) {
                    g++;
                    m = {
                        'id': g,
                        'name': l['name'],
                        'tabs': []
                    };
                    f[g] = m;
                }
            }
            $['each'](l['tabs'], function (n, o) {
                m['tabs']['push']({
                    'title': o['title'],
                    'URL': o['URL'][0x0],
                    'pinned': o['pinned']
                });
            });
        });
        localStorage['rowDataMap'] = JSON['stringify'](f);
        background['afterImport'](0x1);
        alert(getMessage('importedSuccessfully'));
    } catch (k) {
        console['error']('Fail\x20when\x20import', k);
        alert(getMessage('formatError'));
        throw k;
    }
}
$(document)['ready'](function () {
    main();
    $(exportBackup)['click'](function () {
        getExportString();
    });
    $(exportPrint)['click'](function () {
        getExportString(!![]);
    });
    $(importTextToggle)['click'](function () {
        showDiv('importText');
    });
    $(importStorageToggle)['click'](function () {
        LoadFromStorage();
        showDiv('importText');
    });
    $(importButton)['click'](function () {
        importColumn();
    });
    $(mergeButton)['click'](function () {
        mergeTMT();
    });
});