!#/usr/bin/bash

if [ "$(id -u)" = 0 ]; then
    echo "##################################################################"
    echo "This script MUST NOT be run as root user since it makes changes"
    echo "to the \$HOME directory of the \$USER executing this script."
    echo "The \$HOME directory of the root user is, of course, '/root'."
    echo "We don't want to mess around in there. So run this script as a"
    echo "normal user. You will be asked for a sudo password when necessary."
    echo "##################################################################"
    exit 1
fi

error() { \
    clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}


sudo apt update

### Utilities

echo "##################################"
echo "### Installing basic utilities ###"
echo "##################################"

sudo apt install -y git wget gpg zsh fish autoconf intltool solaar

### DEs and WMs

# Xmonad
sudo apt-get install xmonad libghc-xmonad-contrib-dev libghc-xmonad-wallpaper-dev libghc-xmonad-prof libghc-xmonad-extras-dev picom suckless-tools xmobar conky trayer

# Set default shell
echo "*** Setting fish as default shell ***"
echo /usr/bin/zsh | sudo tee -a /etc/shells
echo /usr/bin/fish | sudo tee -a /etc/shells
# chsh -s to /usr/local/bin/fish
chsh -s to /usr/local/bin/zsh

# volumeicon
cd ~
git clone https://github.com/Maato/volumeicon.git
cd volumeicon
./autogen.sh
./configure --prefix=/usr
make
sudo make install
cd ~

### Editors

echo "###############################"
echo "### Installing text editors ###"
echo "###############################"

sudo apt install -y vim neovim emacs

## Font packages

# nerd-fonts
cd ~
git clone https://github.com/ryanoasis/nerd-fonts.git
cd nerd-fonts
./install.sh

cd ~

# Fira code
sudo apt install fonts-firacode

# VS Code

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt install -y apt-transport-https
sudo apt install -y code

rm ~/.config/Code/User/settings.json
touch ~/.config/Code/User/settings.json

echo '{
    "workbench.colorTheme": "Material Theme",
    "workbench.startupEditor": "none",
    "workbench.editor.enablePreview": false,
    "editor.fontFamily": "Fira Code, Menlo, monospace",
    "editor.fontLigatures": true, 
    "editor.bracketPairColorization.enabled": true,
    "haskell.manageHLS": "GHCup"
}' > ~/.config/Code/User/settings.json


### Programming languages

echo "########################################"
echo "### Installing Programming Languages ###"
echo "########################################"

# Rust

echo "*** Rust ***"

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
echo "export PATH=$PATH:~/.cargo/bin" >> ~/.bashrc
echo "export PATH=$PATH:~/.cargo/bin" >> ~/.zshrc
echo "export PATH=$PATH:~/.cargo/bin" >> ~/.config/fish/config.fish


# Haskell

echo "*** Haskell ***"

# sudo apt install ghc haskell-stack cabal-install 
curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh

# Golang

echo "*** Golang ***"

sudo apt install -y golang-go
echo "export GOPATH=$HOME/go" >> ~/.bashrc
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" >> ~/.bashrc
echo "export GOPATH=$HOME/go" >> ~/.zshrc
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" >> ~/.zshrc
echo "export GOPATH=$HOME/go" >> ~/.config/fish/config.fish
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" >> ~/.config/fish/config.fish

# NVM

echo "*** NVM ***"

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
echo "export NVM_DIR=\"$([ -z \"${XDG_CONFIG_HOME-}\" ] && printf %s \"${HOME}/.nvm\" || printf %s \"${XDG_CONFIG_HOME}/nvm\")\" [ -s \"$NVM_DIR/nvm.sh\" ] && \. \"$NVM_DIR/nvm.sh\"" >> ~/.bashrc
echo "export NVM_DIR=\"$([ -z \"${XDG_CONFIG_HOME-}\" ] && printf %s \"${HOME}/.nvm\" || printf %s \"${XDG_CONFIG_HOME}/nvm\")\" [ -s \"$NVM_DIR/nvm.sh\" ] && \. \"$NVM_DIR/nvm.sh\"" >> ~/.zshrc

curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install edc/bass
rm -f ~/.config/fish/functions/nvm.fish
touch ~/.config/fish/functions/nvm.fish
echo "function nvm \n\
	\tbass source ~/.nvm/nvm.sh --no-use ';' nvm $argv\n\
      end\n" >> ~/.config/fish/functions/nvm.fish

# ASDF

# G++

### Install Alacritty

cd ~
git clone https://github.com/alacritty/alacritty.git
cd alacritty

sudo apt -y install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3

cargo build --release

sudo cp target/release/alacritty /usr/local/bin # or anywhere else in $PATH
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
sudo desktop-file-install extra/linux/Alacritty.desktop
sudo update-desktop-database

sudo mkdir -p /usr/local/share/man/man1
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null
gzip -c extra/alacritty-msg.man | sudo tee /usr/local/share/man/man1/alacritty-msg.1.gz > /dev/null

ZDOTDIR="~"
fish_complete_path="~/.config/fish/"

mkdir -p ${ZDOTDIR:-~}/.zsh_functions
echo 'fpath+=${ZDOTDIR:-~}/.zsh_functions' >> ${ZDOTDIR:-~}/.zshrc
cp extra/completions/_alacritty ${ZDOTDIR:-~}/.zsh_functions/_alacritty

echo "source $(pwd)/extra/completions/alacritty.bash" >> ~/.bashrc

mkdir -p $fish_complete_path[1]
cp extra/completions/alacritty.fish $fish_complete_path[1]/alacritty.fish
